DEBIAN EASY PAINLESS BUILDING INFRASTRUCTURE.
INSPIRED BY mini-buildd, but made easy for distributing.

* CLI SETUP
Debian package requirements:
- Python 3.6

* FEATURES
- 

* WEB SETUP
Debian package requirements:
- Django 4.2 LTS
- GNUPG2
- devscripts
- dpkg-dev
- debootstrap (the more recent, the better)
- reprepro (the more recent, the better)
- mailjet backend (for email, see if there's python alternative in debian, maybe smtplib)
- sqlite3 (for database)
- qemu-user-static (suggestion, for building chroots apart from amd64 and i386)
- binfmt-support (for chrooting without copying /usr/bin/qemu-`arch`-static manually into the chroot)

* FEATURES
- Web page. You need to login to upload packages.
- All gpg keys are signed and managed by the server (called builder).
- Nice and fast GUI. Maybe design another one for older browsers (?)
- Backend and most functions are handled by pmdebootstrap client.

* DRAWBACKS
- Autobuilt debian/watch will not be implemented. Newer versions will be notified by email.

* LOGGING
- Logs will have their own model, so we can check everything out, along with its generic status alert.

Sample:
Logs
|
|--- Chroot logs
|
|--- Package upload log

At the top of the page:
> Chroot `name-of-chroot`-`distribution` is being built...
> Building package for `distribution-codename-arch`...

* DJANGO MODELS
- See models in mini-buildd sources. Very good example.

* chroot.py
- See this file in mini-buildd sources. Also very good example at setting everything up.
