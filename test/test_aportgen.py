# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import sys
import pytest
import shutil
import filecmp

import pmd_test
import pmd_test.const
import pmd.aportgen
import pmd.aportgen.core
import pmd.config
import pmd.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    cfg = f"{pmd_test.const.testdata}/channels.cfg"
    sys.argv = ["pmdebootstrap.py", "--config-channels", cfg, "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    args.fork_debian = False
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_aportgen_compare_output(args, tmpdir, monkeypatch):
    # Fake debports folder in tmpdir
    tmpdir = str(tmpdir)
    shutil.copytree(args.debports + "/.git", tmpdir + "/.git")
    args.debports = tmpdir
    os.mkdir(tmpdir + "/cross")
    testdata = pmd_test.const.testdata + "/aportgen"

    # Override get_upstream_aport() to point to testdata
    def func(args, upstream_path, arch=None):
        return testdata + "/debports/main/" + upstream_path
    monkeypatch.setattr(pmd.aportgen.core, "get_upstream_aport", func)

    # Run aportgen and compare output
    pkgnames = ["binutils-armhf", "gcc-armhf"]
    for pkgname in pkgnames:
        pmd.aportgen.generate(args, pkgname)
        path_new = args.debports + "/cross/" + pkgname + "/PKGBUILD"
        path_old = testdata + "/debports/cross/" + pkgname + "/PKGBUILD"
        assert os.path.exists(path_new)
        assert filecmp.cmp(path_new, path_old, False)


def test_aportgen_fork_debian_compare_output(args, tmpdir, monkeypatch):
    # Fake debports folder in tmpdir
    tmpdir = str(tmpdir)
    shutil.copytree(args.debports + "/.git", tmpdir + "/.git")
    args.debports = tmpdir
    os.mkdir(tmpdir + "/temp")
    testdata = pmd_test.const.testdata + "/aportgen"
    args.fork_debian = True

    # Override get_upstream_aport() to point to testdata
    def func(args, upstream_path, arch=None):
        return testdata + "/debports/main/" + upstream_path
    monkeypatch.setattr(pmd.aportgen.core, "get_upstream_aport", func)

    # Run aportgen and compare output
    pkgname = "binutils"
    pmd.aportgen.generate(args, pkgname)
    path_new = args.debports + "/temp/" + pkgname + "/PKGBUILD"
    path_old = testdata + "/debports/temp/" + pkgname + "/PKGBUILD"
    assert os.path.exists(path_new)
    assert filecmp.cmp(path_new, path_old, False)


def test_aportgen(args, tmpdir):
    # Fake debports folder in tmpdir
    testdata = pmd_test.const.testdata
    tmpdir = str(tmpdir)
    shutil.copytree(args.debports + "/.git", tmpdir + "/.git")
    args.debports = tmpdir
    shutil.copy(f"{testdata}/debports.cfg", args.debports)
    os.mkdir(tmpdir + "/cross")

    # Create aportgen folder -> code path where it still exists
    pmd.helpers.run.user(args, ["mkdir", "-p", args.work + "/aportgen"])

    # Generate all valid packages (gcc twice -> different code path)
    pkgnames = ["binutils-armv7", "musl-armv7", "busybox-static-armv7",
                "gcc-armv7", "gcc-armv7"]
    for pkgname in pkgnames:
        pmd.aportgen.generate(args, pkgname)


def test_aportgen_invalid_generator(args):
    with pytest.raises(ValueError) as e:
        pmd.aportgen.generate(args, "pkgname-with-no-generator")
    assert "No generator available" in str(e.value)


def test_aportgen_get_upstream_aport(args, monkeypatch):
    # Fake pmd.parse.pkgbuild()
    def fake_pkgbuild(*args, **kwargs):
        return pkgbuild
    monkeypatch.setattr(pmd.parse, "pkgbuild", fake_pkgbuild)

    # Fake pmd.parse.apkindex.package()
    def fake_package(*args, **kwargs):
        return package
    monkeypatch.setattr(pmd.parse.apkindex, "package", fake_package)

    # Equal version
    func = pmd.aportgen.core.get_upstream_aport
    upstream = "gcc"
    upstream_full = args.work + "/cache_git/debports_upstream/main/" + upstream
    pkgbuild = {"pkgver": "2.0", "pkgrel": "0"}
    package = {"version": "2.0-r0"}
    assert func(args, upstream) == upstream_full

    # PKGBUILD < binary
    pkgbuild = {"pkgver": "1.0", "pkgrel": "0"}
    package = {"version": "2.0-r0"}
    with pytest.raises(RuntimeError) as e:
        func(args, upstream)
    assert str(e.value).startswith("You can update your local checkout with")

    # PKGBUILD > binary
    pkgbuild = {"pkgver": "3.0", "pkgrel": "0"}
    package = {"version": "2.0-r0"}
    with pytest.raises(RuntimeError) as e:
        func(args, upstream)
    assert str(e.value).startswith("You can force an update of your binary")
