# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest

import pmd_test  # noqa
import pmd.config.init


def test_require_programs(monkeypatch):
    func = pmd.config.init.require_programs

    # Nothing missing
    func()

    # Missing program
    invalid = "invalid-program-name-here-asdf"
    monkeypatch.setattr(pmd.config, "required_programs", [invalid])
    with pytest.raises(RuntimeError) as e:
        func()
    assert str(e.value).startswith("Can't find all programs")
