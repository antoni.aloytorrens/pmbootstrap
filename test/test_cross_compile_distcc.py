# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import pytest
import sys

import pmd_test  # noqa
import pmd.build
import pmd.chroot.distccd
import pmd.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    sys.argv = ["pmdebootstrap", "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_cross_compile_distcc(args):
    # Delete old distccd log
    pmd.chroot.distccd.stop(args)
    distccd_log = args.work + "/chroot_native/home/pmos/distccd.log"
    if os.path.exists(distccd_log):
        pmd.helpers.run.root(args, ["rm", distccd_log])

    # Force usage of distcc (no fallback, no ccache)
    args.verbose = True
    args.ccache = False
    args.distcc_fallback = False

    # Compile, print distccd and sshd logs on error
    try:
        pmd.build.package(args, "hello-world", arch="armhf", force=True)
    except RuntimeError:
        print("distccd log:")
        pmd.helpers.run.user(args, ["cat", distccd_log], output="stdout",
                             check=False)
        print("sshd log:")
        sshd_log = args.work + "/chroot_native/home/pmos/.distcc-sshd/log.txt"
        pmd.helpers.run.root(args, ["cat", sshd_log], output="stdout",
                             check=False)
        raise
