# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import pmd_test  # noqa
import pmd.config
import pmd.parse
import pmd.helpers.frontend
import pmd.helpers.logging


def test_build_src_invalid_path():
    sys.argv = ["pmdebootstrap.py", "build", "--src=/invalidpath", "hello-world"]
    args = pmd.parse.arguments()

    with pytest.raises(RuntimeError) as e:
        pmd.helpers.frontend.build(args)
    assert str(e.value).startswith("Invalid path specified for --src:")
