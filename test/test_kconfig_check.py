# Copyright 2022 Antoine Fontaine
# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import pmd_test
import pmd_test.const
import pmd.parse.kconfig


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "kconfig", "check"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_kconfig_check(args):
    # non-existing package
    assert pmd.parse.kconfig.check(args, "non-existing-kernel-package",
                                   must_exist=False) is None

    # basic checks, from easiers to hard-ish
    dir = f"{pmd_test.const.testdata}/kconfig_check/"
    assert not pmd.parse.kconfig.check_file(dir +
                                            "bad-missing-required-option")
    assert pmd.parse.kconfig.check_file(dir + "good")
    assert not pmd.parse.kconfig.check_file(dir + "bad-wrong-option-set")
    assert pmd.parse.kconfig.check_file(dir + "good-anbox",
                                        anbox=True)
    assert not pmd.parse.kconfig.check_file(dir +
                                            "bad-array-missing-some-options",
                                            anbox=True)
    assert pmd.parse.kconfig.check_file(dir + "good-nftables",
                                        nftables=True)
    assert not pmd.parse.kconfig.check_file(dir + "bad-nftables",
                                            nftables=True)
    assert pmd.parse.kconfig.check_file(dir + "good-zram",
                                        zram=True)
    assert pmd.parse.kconfig.check_file(dir + "good-uefi",
                                        uefi=True)
    assert not pmd.parse.kconfig.check_file(dir + "bad-uefi",
                                            uefi=True)

    # tests on real devices
    # *** do not add more of these! ***
    # moving forward, tests in pmdebootstrap.git should become more/completely
    # independent of the currently checked out debports.git (#2105)

    # it's a postmarketOS device, it will have the required options, and
    # supports nftables (with pmd:kconfigcheck-nftables)
    assert pmd.parse.kconfig.check(args, "nokia-n900")

    # supports Anbox (with pmd:kconfigcheck-anbox)
    assert pmd.parse.kconfig.check(args, "postmarketos-allwinner")

    # testing the force param: nokia-n900 will never have anbox support
    assert not pmd.parse.kconfig.check(args, "nokia-n900",
                                       force_anbox_check=True)

    # supports zram (with pmd:kconfigcheck-zram), nftables
    assert pmd.parse.kconfig.check(args, "linux-purism-librem5")
