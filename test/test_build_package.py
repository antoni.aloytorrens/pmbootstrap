# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Tests all functions from pmd.build._package """
import datetime
import glob
import os
import pytest
import shutil
import sys

import pmd_test  # noqa
import pmd.build
import pmd.build._package
import pmd.config
import pmd.config.init
import pmd.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    sys.argv = ["pmdebootstrap", "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def return_none(*args, **kwargs):
    return None


def return_string(*args, **kwargs):
    return "some/random/path.apk"


def return_true(*args, **kwargs):
    return True


def return_false(*args, **kwargs):
    return False


def return_fake_build_depends(*args, **kwargs):
    """
    Fake return value for pmd.build._package.build_depends:
    depends: ["debian-base"], depends_built: []
    """
    return (["debian-base"], [])


def args_patched(monkeypatch, argv):
    monkeypatch.setattr(sys, "argv", argv)
    return pmd.parse.arguments()


def test_skip_already_built():
    func = pmd.build._package.skip_already_built
    assert pmd.helpers.other.cache["built"] == {}
    assert func("test-package", "armhf") is False
    assert pmd.helpers.other.cache["built"] == {"armhf": ["test-package"]}
    assert func("test-package", "armhf") is True


def test_get_pkgbuild(args):
    func = pmd.build._package.get_pkgbuild

    # Valid aport
    pkgname = "postmarketos-base"
    assert func(args, pkgname, "x86_64")["pkgname"] == pkgname

    # Valid binary package
    assert func(args, "debian-base", "x86_64") is None

    # Invalid package
    with pytest.raises(RuntimeError) as e:
        func(args, "invalid-package-name", "x86_64")
    assert "Could not find" in str(e.value)


def test_check_build_for_arch(monkeypatch, args):
    # Fake PKGBUILD data
    pkgbuild = {"pkgname": "testpkgname"}

    def fake_helpers_debports_get(args, pkgname):
        return pkgbuild
    monkeypatch.setattr(pmd.helpers.debports, "get", fake_helpers_debports_get)

    # pmaport with arch exists
    func = pmd.build._package.check_build_for_arch
    pkgbuild["arch"] = ["armhf"]
    assert func(args, "testpkgname", "armhf") is True
    pkgbuild["arch"] = ["noarch"]
    assert func(args, "testpkgname", "armhf") is True
    pkgbuild["arch"] = ["all"]
    assert func(args, "testpkgname", "armhf") is True

    # No binary package exists and can't build it
    pkgbuild["arch"] = ["x86_64"]
    with pytest.raises(RuntimeError) as e:
        func(args, "testpkgname", "armhf")
    assert "Can't build" in str(e.value)

    # pmaport can't be built for x86_64, but binary package exists in Debian
    pkgbuild = {"pkgname": "mesa",
                "arch": "armhf",
                "pkgver": "9999",
                "pkgrel": "0"}
    assert func(args, "mesa", "x86_64") is False


def test_get_depends(monkeypatch):
    func = pmd.build._package.get_depends
    pkgbuild = {"pkgname": "test", "depends": ["a"], "makedepends": ["c", "b"],
                "checkdepends": "e", "subpackages": {"d": None}, "options": []}

    # Depends + makedepends
    args = args_patched(monkeypatch, ["pmdebootstrap", "build", "test"])
    assert func(args, pkgbuild) == ["a", "b", "c", "e"]
    args = args_patched(monkeypatch, ["pmdebootstrap", "install"])
    assert func(args, pkgbuild) == ["a", "b", "c", "e"]

    # Ignore depends (-i)
    args = args_patched(monkeypatch, ["pmdebootstrap", "build", "-i", "test"])
    assert func(args, pkgbuild) == ["b", "c", "e"]

    # Package depends on its own subpackage
    pkgbuild["makedepends"] = ["d"]
    args = args_patched(monkeypatch, ["pmdebootstrap", "build", "test"])
    assert func(args, pkgbuild) == ["a", "e"]

    # Package depends on itself
    pkgbuild["makedepends"] = ["c", "b", "test"]
    args = args_patched(monkeypatch, ["pmdebootstrap", "build", "test"])
    assert func(args, pkgbuild) == ["a", "b", "c", "e"]


def test_build_depends(args, monkeypatch):
    # Shortcut and fake pkgbuild
    func = pmd.build._package.build_depends
    pkgbuild = {"pkgname": "test", "depends": ["a", "!c"],
                "makedepends": ["b"], "checkdepends": [],
                "subpackages": {"d": None}, "options": []}

    # No depends built (first makedepends + depends, then only makedepends)
    monkeypatch.setattr(pmd.build._package, "package", return_none)
    assert func(args, pkgbuild, "armhf", True) == (["!c", "a", "b"], [])

    # All depends built (makedepends only)
    monkeypatch.setattr(pmd.build._package, "package", return_string)
    assert func(args, pkgbuild, "armhf", False) == (["!c", "a", "b"],
                                                    ["a", "b"])


def test_build_depends_no_binary_error(args, monkeypatch):
    # Shortcut and fake pkgbuild
    func = pmd.build._package.build_depends
    pkgbuild = {"pkgname": "test", "depends": ["some-invalid-package-here"],
                "makedepends": [], "checkdepends": [], "subpackages": {},
                "options": []}

    # pmdebootstrap build --no-depends
    args.no_depends = True

    # Missing binary package error
    with pytest.raises(RuntimeError) as e:
        func(args, pkgbuild, "armhf", True)
    assert str(e.value).startswith("Missing binary package for dependency")

    # All depends exist
    pkgbuild["depends"] = ["debian-base"]
    assert func(args, pkgbuild, "armhf", True) == (["debian-base"], [])


def test_build_depends_binary_outdated(args, monkeypatch):
    """ pmdebootstrap runs with --no-depends and dependency binary package is
        outdated (#1895) """
    # Override pmd.parse.apkindex.package(): pretend hello-world is missing
    # and binutils-aarch64 is outdated
    func_orig = pmd.parse.apkindex.package

    def func_patch(args, package, *args2, **kwargs):
        print(f"func_patch: called for package: {package}")
        if package == "hello-world":
            print("pretending that it does not exist")
            return None
        if package == "binutils-aarch64":
            print("pretending that it is outdated")
            ret = func_orig(args, package, *args2, **kwargs)
            ret["version"] = "0-r0"
            return ret
        return func_orig(args, package, *args2, **kwargs)
    monkeypatch.setattr(pmd.parse.apkindex, "package", func_patch)

    # Build hello-world with --no-depends and expect failure
    args.no_depends = True
    pkgname = "hello-world"
    arch = "aarch64"
    force = False
    strict = True
    with pytest.raises(RuntimeError) as e:
        pmd.build.package(args, pkgname, arch, force, strict)
    assert "'binutils-aarch64' of 'gcc-aarch64' is outdated" in str(e.value)


def test_is_necessary_warn_depends(args, monkeypatch):
    # Shortcut and fake pkgbuild
    func = pmd.build._package.is_necessary_warn_depends
    pkgbuild = {"pkgname": "test"}

    # Necessary
    monkeypatch.setattr(pmd.build, "is_necessary", return_true)
    assert func(args, pkgbuild, "armhf", False, []) is True

    # Necessary (strict=True overrides is_necessary())
    monkeypatch.setattr(pmd.build, "is_necessary", return_false)
    assert func(args, pkgbuild, "armhf", True, []) is True

    # Not necessary (with depends: different code path that prints a warning)
    assert func(args, pkgbuild, "armhf", False, []) is False
    assert func(args, pkgbuild, "armhf", False, ["first", "second"]) is False


def test_init_buildenv(args, monkeypatch):
    # First init native chroot buildenv properly without patched functions
    pmd.build.init(args)

    # Disable effects of functions we don't want to test here
    monkeypatch.setattr(pmd.build._package, "build_depends",
                        return_fake_build_depends)
    monkeypatch.setattr(pmd.build._package, "is_necessary_warn_depends",
                        return_true)
    monkeypatch.setattr(pmd.chroot.apk, "install", return_none)
    monkeypatch.setattr(pmd.chroot.distccd, "start", return_none)

    # Shortcut and fake pkgbuild
    func = pmd.build._package.init_buildenv
    pkgbuild = {"pkgname": "test", "depends": ["a"], "makedepends": ["b"],
                "options": []}

    # Build is necessary (various code paths)
    assert func(args, pkgbuild, "armhf", strict=True) is True
    assert func(args, pkgbuild, "armhf", cross="native") is True
    assert func(args, pkgbuild, "armhf", cross="distcc") is True

    # Build is not necessary (only builds dependencies)
    monkeypatch.setattr(pmd.build._package, "is_necessary_warn_depends",
                        return_false)
    assert func(args, pkgbuild, "armhf") is False


def test_get_pkgver(monkeypatch):
    # With original source
    func = pmd.build._package.get_pkgver
    assert func("1.0", True) == "1.0"

    # Without original source
    now = datetime.date(2018, 1, 1)
    assert func("1.0", False, now) == "1.0_p20180101000000"
    assert func("1.0_git20170101", False, now) == "1.0_p20180101000000"


def test_run_abuild(args, monkeypatch):
    # Disable effects of functions we don't want to test here
    monkeypatch.setattr(pmd.build, "copy_to_buildpath", return_none)
    monkeypatch.setattr(pmd.chroot, "user", return_none)

    # Shortcut and fake pkgbuild
    func = pmd.build._package.run_abuild
    pkgbuild = {"pkgname": "test", "pkgver": "1", "pkgrel": "2", "options": []}

    # Normal run
    output = "armhf/test-1-r2.apk"
    env = {"CARCH": "armhf", "SUDO_APK": "abuild-apk --no-progress"}
    cmd = ["abuild", "-D", "postmarketOS", "-d"]
    assert func(args, pkgbuild, "armhf") == (output, cmd, env)

    # Force and strict
    cmd = ["abuild", "-D", "postmarketOS", "-r", "-f"]
    assert func(args, pkgbuild, "armhf", True, True) == (output, cmd, env)

    # cross=native
    env = {"CARCH": "armhf",
           "SUDO_APK": "abuild-apk --no-progress",
           "CROSS_COMPILE": "armv6-debian-linux-musleabihf-",
           "CC": "armv6-debian-linux-musleabihf-gcc"}
    cmd = ["abuild", "-D", "postmarketOS", "-d"]
    assert func(args, pkgbuild, "armhf", cross="native") == (output, cmd, env)

    # cross=distcc
    (output, cmd, env) = func(args, pkgbuild, "armhf", cross="distcc")
    assert output == "armhf/test-1-r2.apk"
    assert env["CARCH"] == "armhf"
    assert env["CCACHE_PREFIX"] == "distcc"
    assert env["CCACHE_PATH"] == "/usr/lib/arch-bin-masquerade/armhf:/usr/bin"
    assert env["CCACHE_COMPILERCHECK"].startswith("string:")
    assert env["DISTCC_HOSTS"] == "@127.0.0.1:/home/pmos/.distcc-sshd/distccd"
    assert env["DISTCC_BACKOFF_PERIOD"] == "0"


def test_finish(args, monkeypatch):
    # Real output path
    output = pmd.build.package(args, "hello-world", force=True)

    # Disable effects of functions we don't want to test below
    monkeypatch.setattr(pmd.chroot, "user", return_none)

    # Shortcut and fake pkgbuild
    func = pmd.build._package.finish
    pkgbuild = {"options": []}

    # Non-existing output path
    with pytest.raises(RuntimeError) as e:
        func(args, pkgbuild, "armhf", "/invalid/path")
    assert "Package not found" in str(e.value)

    # Existing output path
    func(args, pkgbuild, pmd.config.arch_native, output)


def test_package(args):
    # First build
    assert pmd.build.package(args, "hello-world", force=True)

    # Package exists
    pmd.helpers.other.cache["built"] = {}
    assert pmd.build.package(args, "hello-world") is None

    # Force building again
    pmd.helpers.other.cache["built"] = {}
    assert pmd.build.package(args, "hello-world", force=True)

    # Build for another architecture
    assert pmd.build.package(args, "hello-world", "armhf", force=True)

    # Upstream package, for which we don't have an aport
    assert pmd.build.package(args, "debian-base") is None


def test_build_depends_high_level(args, monkeypatch):
    """
    "hello-world-wrapper" depends on "hello-world". We build both, then delete
    "hello-world" and check that it gets rebuilt correctly again.
    """
    # Patch pmd.build.is_necessary() to always build the hello-world package
    def fake_build_is_necessary(args, arch, pkgbuild, apkindex_path=None):
        if pkgbuild["pkgname"] == "hello-world":
            return True
        return pmd.build.other.is_necessary(args, arch, pkgbuild,
                                            apkindex_path)
    monkeypatch.setattr(pmd.build, "is_necessary",
                        fake_build_is_necessary)

    # Build hello-world to get its full output path
    channel = pmd.config.debports.read_config(args)["channel"]
    output_hello = pmd.build.package(args, "hello-world")
    output_hello_outside = f"{args.work}/packages/{channel}/{output_hello}"
    assert os.path.exists(output_hello_outside)

    # Make sure the wrapper exists
    pmd.build.package(args, "hello-world-wrapper")

    # Remove hello-world
    pmd.helpers.run.root(args, ["rm", output_hello_outside])
    pmd.build.index_repo(args, pmd.config.arch_native)
    pmd.helpers.other.cache["built"] = {}

    # Ask to build the wrapper. It should not build the wrapper (it exists, not
    # using force), but build/update its missing dependency "hello-world"
    # instead.
    assert pmd.build.package(args, "hello-world-wrapper") is None
    assert os.path.exists(output_hello_outside)


def test_build_local_source_high_level(args, tmpdir):
    """
    Test building a package with overriding the source code:
        pmdebootstrap build --src=/some/path hello-world

    We use a copy of the hello-world PKGBUILD here that doesn't have the
    source files it needs to build included. And we use the original aport
    folder as local source folder, so pmdebootstrap should take the source files
    from there and the build should succeed.
    """
    # debports: Add deviceinfo (required by pmdebootstrap to start)
    tmpdir = str(tmpdir)
    debports = tmpdir + "/debports"
    aport = debports + "/device/testing/device-" + args.device
    os.makedirs(aport)
    path_original = pmd.helpers.debports.find(args, f"device-{args.device}")
    shutil.copy(f"{path_original}/deviceinfo", aport)

    # debports: Add modified hello-world aport (source="", uses $builddir)
    aport = debports + "/main/hello-world"
    os.makedirs(aport)
    shutil.copy(pmd.config.pmd_src + "/test/testdata/build_local_src/PKGBUILD",
                aport)

    # debports: Add debports.cfg, .git
    shutil.copy(args.debports + "/debports.cfg", debports)
    shutil.copytree(args.debports + "/.git", debports + "/.git")

    # src: Copy hello-world source files
    src = tmpdir + "/src"
    os.makedirs(src)
    shutil.copy(args.debports + "/main/hello-world/Makefile", src)
    shutil.copy(args.debports + "/main/hello-world/main.c", src)

    # src: Create unreadable file (rsync should skip it)
    unreadable = src + "/_unreadable_file"
    shutil.copy(args.debports + "/main/hello-world/main.c", unreadable)
    pmd.helpers.run.root(args, ["chown", "root:root", unreadable])
    pmd.helpers.run.root(args, ["chmod", "500", unreadable])

    # Test native arch and foreign arch chroot
    channel = pmd.config.debports.read_config(args)["channel"]
    for arch in [pmd.config.arch_native, "armhf"]:
        # Delete all hello-world --src packages
        pattern = f"{args.work}/packages/{channel}/{arch}/hello-world-*_p*.apk"
        for path in glob.glob(pattern):
            pmd.helpers.run.root(args, ["rm", path])
        assert len(glob.glob(pattern)) == 0

        # Build hello-world --src package
        pmd.helpers.run.user(args, [pmd.config.pmd_src + "/pmdebootstrap.py",
                                    "--debports", debports, "build", "--src", src,
                                    "hello-world", "--arch", arch])

        # Verify that the package has been built and delete it
        paths = glob.glob(pattern)
        assert len(paths) == 1
        pmd.helpers.run.root(args, ["rm", paths[0]])

    # Clean up: update index, delete temp folder
    pmd.build.index_repo(args, pmd.config.arch_native)
    pmd.helpers.run.root(args, ["rm", "-r", tmpdir])
