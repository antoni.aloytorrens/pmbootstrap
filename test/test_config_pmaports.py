# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test pmd/config/debports.py """
import pytest
import sys

import pmd_test
import pmd_test.const
import pmd_test.git
import pmd.config
import pmd.config.workdir
import pmd.config.debports


@pytest.fixture
def args(request):
    import pmd.parse
    cfg = f"{pmd_test.const.testdata}/channels.cfg"
    sys.argv = ["pmdebootstrap.py", "--config-channels", cfg, "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_switch_to_channel_branch(args, monkeypatch, tmpdir):
    path, run_git = pmd_test.git.prepare_tmpdir(args, monkeypatch, tmpdir)
    args.debports = path

    # Pretend to have channel=edge in debports.cfg
    def read_config(args):
        return {"channel": "edge"}
    monkeypatch.setattr(pmd.config.debports, "read_config", read_config)

    # Success: Channel does not change
    func = pmd.config.debports.switch_to_channel_branch
    assert func(args, "edge") is False

    # Fail: git error (could be any error, but here: branch does not exist)
    with pytest.raises(RuntimeError) as e:
        func(args, "v20.05")
    assert str(e.value).startswith("Failed to switch branch")

    # Success: switch channel and change branch
    run_git(["checkout", "-b", "v20.05"])
    run_git(["checkout", "master"])
    assert func(args, "v20.05") is True
    branch = pmd.helpers.git.rev_parse(args, path, extra_args=["--abbrev-ref"])
    assert branch == "v20.05"


def test_read_config_channel(args, monkeypatch):
    channel = "edge"

    # Pretend to have a certain channel in debports.cfg
    def read_config(args):
        return {"channel": channel}
    monkeypatch.setattr(pmd.config.debports, "read_config", read_config)

    # Channel found
    func = pmd.config.debports.read_config_channel
    exp = {"description": "Rolling release channel",
           "branch_debports": "master",
           "branch_debports": "master",
           "mirrordir_debian": "edge"}
    assert func(args) == exp

    # Channel not found
    channel = "non-existing"
    with pytest.raises(RuntimeError) as e:
        func(args)
    assert "channel was not found in channels.cfg" in str(e.value)
