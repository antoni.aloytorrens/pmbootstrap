# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pmd.config


testdata = pmd.config.pmd_src + "/test/testdata"
