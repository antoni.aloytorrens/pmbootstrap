# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import pytest
import sys
import shutil

import pmd_test  # noqa
import pmd_test.const
import pmd.aportgen
import pmd.config
import pmd.helpers.logging
import pmd.parse


@pytest.fixture
def args(tmpdir, request):
    cfg = f"{pmd_test.const.testdata}/channels.cfg"
    sys.argv = ["pmdebootstrap.py", "--config-channels", cfg, "build", "-i",
                "device-testsuite-testdevice"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)

    # Fake debports folder:
    tmpdir = str(tmpdir)
    shutil.copytree(args.debports + "/.git", tmpdir + "/.git")
    setattr(args, "_debports_real", args.debports)
    args.debports = tmpdir

    # Copy the devicepkg-dev package (shared device-* PKGBUILD code)
    pmd.helpers.run.user(args, ["mkdir", "-p", tmpdir + "/main"])
    path_dev = args._debports_real + "/main/devicepkg-dev"
    pmd.helpers.run.user(args, ["cp", "-r", path_dev, tmpdir + "/main"])

    # Copy the linux-lg-mako aport (we currently copy patches from there)
    pmd.helpers.run.user(args, ["mkdir", "-p", tmpdir + "/device/testing"])
    path_mako = args._debports_real + "/device/testing/linux-lg-mako"
    pmd.helpers.run.user(args, ["cp", "-r", path_mako,
                                f"{tmpdir}/device/testing"])

    # Copy debports.cfg
    shutil.copy(f"{pmd_test.const.testdata}/debports.cfg", args.debports)
    return args


def generate(args, monkeypatch, answers):
    """
    Generate the device-new-device and linux-new-device debports (with a patched
    pmd.helpers.cli()).

    :returns: (deviceinfo, pkgbuild, pkgbuild_linux) - the parsed dictionaries
              of the created files, as returned by pmd.parse.pkgbuild() and
              pmd.parse.deviceinfo().
    """
    # Patched function
    def fake_ask(question="Continue?", choices=["y", "n"], default="n",
                 lowercase_answer=True, validation_regex=None, complete=None):
        for substr, answer in answers.items():
            if substr in question:
                logging.info(question + ": " + answer)
                # raise RuntimeError("test>" + answer)
                return answer
        raise RuntimeError("This testcase didn't expect the question '" +
                           question + "', please add it to the mapping.")

    # Generate the debports
    monkeypatch.setattr(pmd.helpers.cli, "ask", fake_ask)
    pmd.aportgen.generate(args, "device-testsuite-testdevice")
    pmd.aportgen.generate(args, "linux-testsuite-testdevice")
    monkeypatch.undo()

    pkgbuild_path = (f"{args.debports}/device/testing/"
                     "device-testsuite-testdevice/PKGBUILD")
    pkgbuild_path_linux = (args.debports + "/device/testing/"
                           "linux-testsuite-testdevice/PKGBUILD")

    # The build fails if the email is not a valid email, so remove them just
    # for tests
    remove_contributor_maintainer_lines(args, pkgbuild_path)
    remove_contributor_maintainer_lines(args, pkgbuild_path_linux)

    # Parse the deviceinfo and pkgbuilds
    pmd.helpers.other.cache["pkgbuild"] = {}
    pkgbuild = pmd.parse.pkgbuild(pkgbuild_path)
    pkgbuild_linux = pmd.parse.pkgbuild(pkgbuild_path_linux,
                                        check_pkgver=False)
    deviceinfo = pmd.parse.deviceinfo(args, "testsuite-testdevice")
    return (deviceinfo, pkgbuild, pkgbuild_linux)


def remove_contributor_maintainer_lines(args, path):
    with open(path, "r+", encoding="utf-8") as handle:
        lines_new = []
        for line in handle.readlines():
            # Skip maintainer/contributor
            if line.startswith("# Maintainer") or line.startswith(
                    "# Contributor"):
                continue
            lines_new.append(line)
        # Write back
        handle.seek(0)
        handle.write("".join(lines_new))
        handle.truncate()


def test_aportgen_device_wizard(args, monkeypatch):
    """
    Generate a device-testsuite-testdevice and linux-testsuite-testdevice
    package multiple times and check if the output is correct. Also build the
    device package once.
    """
    # Answers to interactive questions
    answers = {
        "Device architecture": "armv7",
        "external storage": "y",
        "hardware keyboard": "n",
        "Flash method": "heimdall",
        "Manufacturer": "Testsuite",
        "Name": "Testsuite Testdevice",
        "Year": "1337",
        "Chassis": "handset",
        "Type": "isorec",
    }

    # First run
    deviceinfo, pkgbuild, pkgbuild_linux = generate(args, monkeypatch, answers)
    assert pkgbuild["pkgname"] == "device-testsuite-testdevice"
    assert pkgbuild["pkgdesc"] == "Testsuite Testdevice"
    assert pkgbuild["depends"] == ["linux-testsuite-testdevice",
                                   "mesa-dri-gallium",
                                   "postmarketos-base"]

    assert pkgbuild_linux["pkgname"] == "linux-testsuite-testdevice"
    assert pkgbuild_linux["pkgdesc"] == "Testsuite Testdevice kernel fork"
    assert pkgbuild_linux["arch"] == ["armv7"]
    assert pkgbuild_linux["_flavor"] == "testsuite-testdevice"

    assert deviceinfo["name"] == "Testsuite Testdevice"
    assert deviceinfo["manufacturer"] == answers["Manufacturer"]
    assert deviceinfo["arch"] == "armv7"
    assert deviceinfo["year"] == "1337"
    assert deviceinfo["chassis"] == "handset"
    assert deviceinfo["keyboard"] == "false"
    assert deviceinfo["external_storage"] == "true"
    assert deviceinfo["flash_method"] == "heimdall-isorec"
    assert deviceinfo["generate_bootimg"] == ""
    assert deviceinfo["generate_legacy_uboot_initfs"] == ""

    # Build the device package
    pkgname = "device-testsuite-testdevice"
    pmd.build.checksum.update(args, pkgname)
    pmd.build.package(args, pkgname, "armv7", force=True)

    # Abort on overwrite confirmation
    answers["overwrite"] = "n"
    with pytest.raises(RuntimeError) as e:
        deviceinfo, pkgbuild, pkgbuild_linux = generate(args, monkeypatch,
                                                        answers)
    assert "Aborted." in str(e.value)

    # fastboot (mkbootimg)
    answers["overwrite"] = "y"
    answers["Flash method"] = "fastboot"
    answers["Path"] = ""
    deviceinfo, pkgbuild, pkgbuild_linux = generate(args, monkeypatch, answers)
    assert pkgbuild["depends"] == ["linux-testsuite-testdevice",
                                   "mesa-dri-gallium",
                                   "mkbootimg",
                                   "postmarketos-base"]

    assert deviceinfo["flash_method"] == answers["Flash method"]
    assert deviceinfo["generate_bootimg"] == "true"

    # 0xffff (legacy uboot initfs)
    answers["Flash method"] = "0xffff"
    deviceinfo, pkgbuild, pkgbuild_linux = generate(args, monkeypatch, answers)
    assert pkgbuild["depends"] == ["linux-testsuite-testdevice",
                                   "mesa-dri-gallium",
                                   "postmarketos-base",
                                   "uboot-tools"]

    assert deviceinfo["generate_legacy_uboot_initfs"] == "true"
