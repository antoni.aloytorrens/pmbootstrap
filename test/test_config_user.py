# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import pmd_test  # noqa
import pmd.aportgen
import pmd.config
import pmd.helpers.frontend
import pmd.helpers.logging
import pmd.helpers.run


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def change_config(monkeypatch, path_config, key, value):
    args = args_patched(monkeypatch, ["pmdebootstrap.py", "-c", path_config,
                                      "config", key, value])
    pmd.helpers.frontend.config(args)


def args_patched(monkeypatch, argv):
    monkeypatch.setattr(sys, "argv", argv)
    return pmd.parse.arguments()


def test_config_user(args, tmpdir, monkeypatch):
    # Temporary paths
    tmpdir = str(tmpdir)
    path_work = tmpdir + "/work"
    path_config = tmpdir + "/pmdebootstrap.cfg"

    # Generate default config (only uses tmpdir)
    cmd = pmd.helpers.run.flat_cmd(["./pmdebootstrap.py",
                                    "-c", path_config,
                                    "-w", path_work,
                                    "--debports", args.debports,
                                    "init"])
    pmd.helpers.run.user(args, ["sh", "-c", "yes '' | " + cmd],
                         pmd.config.pmd_src)

    # Load and verify default config
    argv = ["pmdebootstrap.py", "-c", path_config, "config"]
    args_default = args_patched(monkeypatch, argv)
    assert args_default.work == path_work

    # Modify jobs count
    change_config(monkeypatch, path_config, "jobs", "9000")
    assert args_patched(monkeypatch, argv).jobs == "9000"

    # Override jobs count via commandline (-j)
    argv_jobs = ["pmdebootstrap.py", "-c", path_config, "-j", "1000", "config"]
    assert args_patched(monkeypatch, argv_jobs).jobs == "1000"

    # Override a config option with something that evaluates to false
    argv_empty = ["pmdebootstrap.py", "-c", path_config, "-w", "",
                  "--details-to-stdout", "config"]
    assert args_patched(monkeypatch, argv_empty).work == ""
