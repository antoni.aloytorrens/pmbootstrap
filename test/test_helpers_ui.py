# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import pmd_test
import pmd_test.const
import pmd.helpers.logging
import pmd.helpers.ui


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    cfg = f"{pmd_test.const.testdata}/channels.cfg"
    sys.argv = ["pmdebootstrap.py", "--config-channels", cfg, "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_helpers_ui(args):
    """ Test the UIs returned by pmd.helpers.ui.list() with a testdata debports
        dir. That test dir has a plasma-mobile UI, which is disabled for armhf,
        so it must not be returned when querying the UI list for armhf. """
    args.debports = f"{pmd_test.const.testdata}/helpers_ui/debports"
    func = pmd.helpers.ui.list
    none_desc = "Bare minimum OS image for testing and manual" \
                " customization. The \"console\" UI should be selected if" \
                " a graphical UI is not desired."
    assert func(args, "armhf") == [("none", none_desc)]
    assert func(args, "x86_64") == [("none", none_desc),
                                    ("plasma-mobile", "cool pkgdesc")]
