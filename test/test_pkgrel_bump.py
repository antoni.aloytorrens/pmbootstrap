# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test pmd.helper.pkgrel_bump """
import glob
import os
import pytest
import shutil
import sys

import pmd_test  # noqa
import pmd.helpers.pkgrel_bump
import pmd.helpers.logging


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def pmdebootstrap(args, tmpdir, parameters, zero_exit=True):
    """
    Helper function for running pmdebootstrap inside the fake work folder
    (created by setup() below) with the binary repo disabled and with the
    testdata configured as debports.

    :param parameters: what to pass to pmdebootstrap, e.g. ["build", "testlib"]
    :param zero_exit: expect pmdebootstrap to exit with 0 (no error)
    """
    # Run pmdebootstrap
    debports = tmpdir + "/_debports"
    config = tmpdir + "/_pmdebootstrap.cfg"

    # Copy .git dir to fake debports
    dot_git = tmpdir + "/_debports/.git"
    if not os.path.exists(dot_git):
        shutil.copytree(args.debports + "/.git", dot_git)

    try:
        pmd.helpers.run.user(args, ["./pmdebootstrap.py", "--work=" + tmpdir,
                                    "--mirror-pmOS=", "--debports=" + debports,
                                    "--config=" + config] + parameters,
                             working_dir=pmd.config.pmd_src)

    # Verify that it exits as desired
    except Exception as exc:
        if zero_exit:
            raise RuntimeError("pmdebootstrap failed") from exc
        else:
            return
    if not zero_exit:
        raise RuntimeError("Expected pmdebootstrap to fail, but it did not!")


def setup_work(args, tmpdir):
    """
    Create fake work folder in tmpdir with everything symlinked except for the
    built packages. The debports testdata gets copied to the tempfolder as
    well, so it can be modified during testing.
    """
    # Clean the chroots, and initialize the build chroot in the native chroot.
    # We do this before creating the fake work folder, because then all
    # packages are still present.
    os.chdir(pmd.config.pmd_src)
    pmd.helpers.run.user(args, ["./pmdebootstrap.py", "-y", "zap"])
    pmd.helpers.run.user(args, ["./pmdebootstrap.py", "build_init"])
    pmd.helpers.run.user(args, ["./pmdebootstrap.py", "shutdown"])

    # Link everything from work (except for "packages") to the tmpdir
    for path in glob.glob(args.work + "/*"):
        if os.path.basename(path) != "packages":
            pmd.helpers.run.user(args, ["ln", "-s", path, tmpdir + "/"])

    # Copy testdata and selected device aport
    for folder in ["device/testing", "main"]:
        pmd.helpers.run.user(args, ["mkdir", "-p", args.debports, tmpdir +
                                    "/_debports/" + folder])
    path_original = pmd.helpers.debports.find(args, f"device-{args.device}")
    pmd.helpers.run.user(args, ["cp", "-r", path_original,
                                f"{tmpdir}/_debports/device/testing"])
    for pkgname in ["testlib", "testapp", "testsubpkg"]:
        pmd.helpers.run.user(args, ["cp", "-r",
                                    "test/testdata/pkgrel_bump/debports/"
                                    f"{pkgname}",
                                    f"{tmpdir}/_debports/main/{pkgname}"])

    # Copy debports.cfg
    pmd.helpers.run.user(args, ["cp", args.debports + "/debports.cfg", tmpdir +
                                "/_debports"])

    # Empty packages folder
    channel = pmd.config.debports.read_config(args)["channel"]
    packages_path = f"{tmpdir}/packages/{channel}"
    pmd.helpers.run.user(args, ["mkdir", "-p", packages_path])
    pmd.helpers.run.user(args, ["chmod", "777", packages_path])

    # Copy over the pmdebootstrap config
    pmd.helpers.run.user(args, ["cp", args.config, tmpdir +
                                "/_pmdebootstrap.cfg"])


def verify_pkgrels(tmpdir, pkgrel_testlib, pkgrel_testapp,
                   pkgrel_testsubpkg):
    """
    Verify the pkgrels of the three test PKGBUILDs ("testlib", "testapp",
    "testsubpkg").
    """
    pmd.helpers.other.cache["pkgbuild"] = {}
    mapping = {"testlib": pkgrel_testlib,
               "testapp": pkgrel_testapp,
               "testsubpkg": pkgrel_testsubpkg}
    for pkgname, pkgrel in mapping.items():
        # PKGBUILD path
        path = tmpdir + "/_debports/main/" + pkgname + "/PKGBUILD"

        # Parse and verify
        pkgbuild = pmd.parse.pkgbuild(path)
        assert pkgrel == int(pkgbuild["pkgrel"])


def test_pkgrel_bump_high_level(args, tmpdir):
    # Tempdir setup
    tmpdir = str(tmpdir)
    setup_work(args, tmpdir)

    # Let pkgrel_bump exit normally
    pmdebootstrap(args, tmpdir, ["build", "testlib", "testapp", "testsubpkg"])
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"])
    verify_pkgrels(tmpdir, 0, 0, 0)

    # Increase soname (testlib soname changes with the pkgrel)
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "testlib"])
    verify_pkgrels(tmpdir, 1, 0, 0)
    pmdebootstrap(args, tmpdir, ["build", "testlib"])
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"])
    verify_pkgrels(tmpdir, 1, 0, 0)

    # Delete package with previous soname (--auto-dry exits with >0 now)
    channel = pmd.config.debports.read_config(args)["channel"]
    arch = pmd.config.arch_native
    apk_path = f"{tmpdir}/packages/{channel}/{arch}/testlib-1.0-r0.apk"
    pmd.helpers.run.root(args, ["rm", apk_path])
    pmdebootstrap(args, tmpdir, ["index"])
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"], False)
    verify_pkgrels(tmpdir, 1, 0, 0)

    # Bump pkgrel and build testapp/testsubpkg
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "--auto"])
    verify_pkgrels(tmpdir, 1, 1, 1)
    pmdebootstrap(args, tmpdir, ["build", "testapp", "testsubpkg"])

    # After rebuilding, pkgrel_bump --auto-dry exits with 0
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"])
    verify_pkgrels(tmpdir, 1, 1, 1)

    # Test running with specific package names
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "invalid_package_name"], False)
    pmdebootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "testlib"], False)
    verify_pkgrels(tmpdir, 1, 1, 1)

    # Clean up
    pmdebootstrap(args, tmpdir, ["shutdown"])
    pmd.helpers.run.root(args, ["rm", "-rf", tmpdir])
