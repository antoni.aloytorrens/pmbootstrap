# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import pmd_test  # noqa
import pmd.chroot.root
import pmd.chroot.user
import pmd.helpers.run
import pmd.helpers.logging


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_shell_escape(args):
    cmds = {"test\n": ["echo", "test"],
            "test && test\n": ["echo", "test", "&&", "test"],
            "test ; test\n": ["echo", "test", ";", "test"],
            "'test\"test\\'\n": ["echo", "'test\"test\\'"],
            "*\n": ["echo", "*"],
            "$PWD\n": ["echo", "$PWD"],
            "hello world\n": ["printf", "%s world\n", "hello"]}
    for expected, cmd in cmds.items():
        copy = list(cmd)
        core = pmd.helpers.run_core.core(args, str(cmd), cmd,
                                         output_return=True)
        assert expected == core
        assert cmd == copy

        user = pmd.helpers.run.user(args, cmd, output_return=True)
        assert expected == user
        assert cmd == copy

        root = pmd.helpers.run.root(args, cmd, output_return=True)
        assert expected == root
        assert cmd == copy

        chroot_root = pmd.chroot.root(args, cmd, output_return=True)
        assert expected == chroot_root
        assert cmd == copy

        chroot_user = pmd.chroot.user(args, cmd, output_return=True)
        assert expected == chroot_user
        assert cmd == copy


def test_shell_escape_env(args):
    key = "PMBOOTSTRAP_TEST_ENVIRONMENT_VARIABLE"
    value = "long value with spaces and special characters: '\"\\!$test"
    env = {key: value}
    cmd = ["sh", "-c", "env | grep " + key + " | grep -v SUDO_COMMAND"]
    ret = key + "=" + value + "\n"

    copy = list(cmd)
    func = pmd.helpers.run.user
    assert func(args, cmd, output_return=True, env=env) == ret
    assert cmd == copy

    func = pmd.helpers.run.root
    assert func(args, cmd, output_return=True, env=env) == ret
    assert cmd == copy

    func = pmd.chroot.root
    assert func(args, cmd, output_return=True, env=env) == ret
    assert cmd == copy

    func = pmd.chroot.user
    assert func(args, cmd, output_return=True, env=env) == ret
    assert cmd == copy


def test_flat_cmd_simple():
    func = pmd.helpers.run.flat_cmd
    cmd = ["echo", "test"]
    working_dir = None
    ret = "echo test"
    env = {}
    assert func(cmd, working_dir, env) == ret


def test_flat_cmd_wrap_shell_string_with_spaces():
    func = pmd.helpers.run.flat_cmd
    cmd = ["echo", "string with spaces"]
    working_dir = None
    ret = "echo 'string with spaces'"
    env = {}
    assert func(cmd, working_dir, env) == ret


def test_flat_cmd_wrap_env_simple():
    func = pmd.helpers.run.flat_cmd
    cmd = ["echo", "test"]
    working_dir = None
    ret = "JOBS=5 echo test"
    env = {"JOBS": "5"}
    assert func(cmd, working_dir, env) == ret


def test_flat_cmd_wrap_env_spaces():
    func = pmd.helpers.run.flat_cmd
    cmd = ["echo", "test"]
    working_dir = None
    ret = "JOBS=5 TEST='spaces string' echo test"
    env = {"JOBS": "5", "TEST": "spaces string"}
    assert func(cmd, working_dir, env) == ret
