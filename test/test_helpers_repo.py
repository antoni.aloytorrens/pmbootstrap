# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test pmd.helpers.repo """
import pytest
import sys

import pmd_test  # noqa
import pmd_test.const
import pmd.helpers.repo


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    cfg = f"{pmd_test.const.testdata}/channels.cfg"
    sys.argv = ["pmdebootstrap.py", "--config-channels", cfg, "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_hash():
    url = "https://nl.debianlinux.org/debian/edge/testing"
    hash = "865a153c"
    assert pmd.helpers.repo.hash(url, 8) == hash


def test_debian_apkindex_path(args):
    func = pmd.helpers.repo.debian_apkindex_path
    args.mirror_debian = "http://dl-cdn.debianlinux.org/debian/"
    ret = args.work + "/cache_apk_armhf/APKINDEX.30e6f5af.tar.gz"
    assert func(args, "testing", "armhf") == ret


def test_urls(args, monkeypatch):
    func = pmd.helpers.repo.urls
    channel = "v20.05"
    args.mirror_debian = "http://localhost/debian/"

    # Second mirror with /master at the end is legacy, gets fixed by func.
    # Note that bpo uses multiple postmarketOS mirrors at the same time, so it
    # can use its WIP repository together with the final repository.
    args.mirrors_postmarketos = ["http://localhost/pmos1/",
                                 "http://localhost/pmos2/master"]

    # Pretend to have a certain channel in debports.cfg
    def read_config(args):
        return {"channel": channel}
    monkeypatch.setattr(pmd.config.debports, "read_config", read_config)

    # Channel: v20.05
    assert func(args) == ["/mnt/pmdebootstrap-packages",
                          "http://localhost/pmos1/v20.05",
                          "http://localhost/pmos2/v20.05",
                          "http://localhost/debian/v3.11/main",
                          "http://localhost/debian/v3.11/community"]

    # Channel: edge (has Debian's testing)
    channel = "edge"
    assert func(args) == ["/mnt/pmdebootstrap-packages",
                          "http://localhost/pmos1/master",
                          "http://localhost/pmos2/master",
                          "http://localhost/debian/edge/main",
                          "http://localhost/debian/edge/community",
                          "http://localhost/debian/edge/testing"]

    # Only Debian's URLs
    exp = ["http://localhost/debian/edge/main",
           "http://localhost/debian/edge/community",
           "http://localhost/debian/edge/testing"]
    assert func(args, False, False) == exp
