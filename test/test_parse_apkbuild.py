# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import pmd_test
import pmd_test.const
import pmd.parse._pkgbuild


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_subpackages():
    testdata = pmd_test.const.testdata
    path = testdata + "/pkgbuild/PKGBUILD.subpackages"
    pkgbuild = pmd.parse.pkgbuild(path, check_pkgname=False)

    subpkg = pkgbuild["subpackages"]["simple"]
    assert subpkg["pkgdesc"] == ""
    # Inherited from parent package
    assert subpkg["depends"] == ["postmarketos-base"]

    subpkg = pkgbuild["subpackages"]["custom"]
    assert subpkg["pkgdesc"] == "This is one of the custom subpackages"
    assert subpkg["depends"] == ["postmarketos-base", "glibc"]

    # Successful extraction
    path = (testdata + "/init_questions_device/debports/device/testing/"
            "device-nonfree-firmware/PKGBUILD")
    pkgbuild = pmd.parse.pkgbuild(path)
    subpkg = (pkgbuild["subpackages"]
              ["device-nonfree-firmware-nonfree-firmware"])
    assert subpkg["pkgdesc"] == "firmware description"

    # Can't find the pkgdesc in the function
    path = testdata + "/pkgbuild/PKGBUILD.missing-pkgdesc-in-subpackage"
    pkgbuild = pmd.parse.pkgbuild(path, check_pkgname=False)
    subpkg = (pkgbuild["subpackages"]
              ["missing-pkgdesc-in-subpackage-subpackage"])
    assert subpkg["pkgdesc"] == ""

    # Can't find the function
    assert pkgbuild["subpackages"]["invalid-function"] is None


def test_kernels(args):
    # Kernel hardcoded in depends
    args.debports = pmd_test.const.testdata + "/init_questions_device/debports"
    func = pmd.parse._pkgbuild.kernels
    device = "lg-mako"
    assert func(args, device) is None

    # Upstream and downstream kernel
    device = "sony-amami"
    ret = {"downstream": "Downstream description",
           "mainline": "Mainline description"}
    assert func(args, device) == ret

    # Long kernel name (e.g. two different mainline kernels)
    device = "wileyfox-crackling"
    ret = {"mainline": "Mainline kernel (no modem)",
           "mainline-modem": "Mainline kernel (with modem)",
           "downstream": "Downstream kernel"}
    assert func(args, device) == ret


def test_depends_in_depends():
    path = pmd_test.const.testdata + "/pkgbuild/PKGBUILD.depends-in-depends"
    pkgbuild = pmd.parse.pkgbuild(path, check_pkgname=False)
    assert pkgbuild["depends"] == ["first", "second", "third"]


def test_parse_attributes():
    # Convenience function for calling the function with a block of text
    def func(attribute, block):
        lines = block.split("\n")
        for i in range(0, len(lines)):
            lines[i] += "\n"
        i = 0
        path = "(testcase in " + __file__ + ")"
        print("=== parsing attribute '" + attribute + "' in test block:")
        print(block)
        print("===")
        return pmd.parse._pkgbuild.parse_attribute(attribute, lines, i, path)

    assert func("depends", "pkgname='test'") == (False, None, 0)

    assert func("pkgname", 'pkgname="test"') == (True, "test", 0)

    assert func("pkgname", "pkgname='test'") == (True, "test", 0)

    assert func("pkgname", "pkgname=test") == (True, "test", 0)

    assert func("pkgname", 'pkgname="test\n"') == (True, "test", 1)

    assert func("pkgname", 'pkgname="\ntest\n"') == (True, "test", 2)

    assert func("pkgname", 'pkgname="test" # random comment\npkgrel=3') == \
        (True, "test", 0)

    assert func("pkgver", 'pkgver=2.37 # random comment\npkgrel=3') == \
           (True, "2.37", 0)

    assert func("depends", "depends='\nfirst\nsecond\nthird\n'#") == \
        (True, "first second third", 4)

    assert func("depends", 'depends="\nfirst\n\tsecond third"') == \
        (True, "first second third", 2)

    assert func("depends", 'depends=') == (True, "", 0)

    with pytest.raises(RuntimeError) as e:
        func("depends", 'depends="\nmissing\nend\nquote\nsign')
    assert str(e.value).startswith("Can't find closing")

    with pytest.raises(RuntimeError) as e:
        func("depends", 'depends="')
    assert str(e.value).startswith("Can't find closing")


def test_variable_replacements():
    path = pmd_test.const.testdata + "/pkgbuild/PKGBUILD.variable-replacements"
    pkgbuild = pmd.parse.pkgbuild(path, check_pkgname=False)
    assert pkgbuild["pkgdesc"] == "this should not affect variable replacement"
    assert pkgbuild["url"] == "replacements variable string-replacements"
    assert list(pkgbuild["subpackages"].keys()) == ["replacements", "test"]

    assert pkgbuild["subpackages"]["replacements"] is None
    test_subpkg = pkgbuild["subpackages"]["test"]
    assert test_subpkg["pkgdesc"] == ("this should not affect variable "
                                      "replacement")


def test_parse_maintainers():
    path = pmd_test.const.testdata + "/pkgbuild/PKGBUILD.lint"
    maintainers = [
        "Oliver Smith <ollieparanoid@postmarketos.org>",
        "Hello World <hello@world>"
    ]

    assert pmd.parse._pkgbuild.maintainers(path) == maintainers


def test_parse_unmaintained():
    path = (f"{pmd_test.const.testdata}/pkgbuild"
            "/PKGBUILD.missing-pkgdesc-in-subpackage")
    assert pmd.parse._pkgbuild.unmaintained(path) == "This is broken!"
