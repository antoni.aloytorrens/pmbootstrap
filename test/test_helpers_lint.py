# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import pytest
import shutil
import sys

import pmd_test
import pmd_test.const
import pmd.helpers.lint
import pmd.helpers.run


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap", "lint"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_pmdebootstrap_lint(args, tmpdir):
    args.debports = tmpdir = str(tmpdir)

    # Create hello-world pmaport in tmpdir
    pkgbuild_orig = f"{pmd_test.const.testdata}/pkgbuild/PKGBUILD.lint"
    pkgbuild_tmp = f"{tmpdir}/hello-world/PKGBUILD"
    os.makedirs(f"{tmpdir}/hello-world")
    shutil.copyfile(pkgbuild_orig, pkgbuild_tmp)

    # Lint passes
    assert pmd.helpers.lint.check(args, ["hello-world"]) == ""

    # Change "pmd:cross-native" to non-existing "pmd:invalid-opt"
    pmd.helpers.run.user(args, ["sed", "s/pmd:cross-native/pmd:invalid-opt/g",
                                "-i", pkgbuild_tmp])

    # Lint error
    err_str = "invalid option 'pmd:invalid-opt'"
    assert err_str in pmd.helpers.lint.check(args, ["hello-world"])
