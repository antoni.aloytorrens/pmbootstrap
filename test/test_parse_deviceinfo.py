# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import pmd_test.const
import pmd.parse


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_kernel_suffix(args):
    args.debports = pmd_test.const.testdata + "/deviceinfo/debports"
    device = "multiple-kernels"

    kernel = "mainline"
    deviceinfo = pmd.parse.deviceinfo(args, device, kernel)
    assert deviceinfo["append_dtb"] == "yes"
    assert deviceinfo["dtb"] == "mainline-dtb"

    kernel = "mainline-modem"
    deviceinfo = pmd.parse.deviceinfo(args, device, kernel)
    assert deviceinfo["append_dtb"] == "yes"
    assert deviceinfo["dtb"] == "mainline-modem-dtb"

    kernel = "downstream"
    deviceinfo = pmd.parse.deviceinfo(args, device, kernel)
    assert deviceinfo["append_dtb"] == "yes"
    assert deviceinfo["dtb"] == "downstream-dtb"
