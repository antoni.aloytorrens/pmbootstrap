# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import pytest
import sys

import pmd_test  # noqa
import pmd.build.other


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap", "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_guess_main(args, tmpdir):
    # Fake debports folder
    tmpdir = str(tmpdir)
    args.debports = tmpdir
    for aport in ["temp/qemu", "main/some-pkg"]:
        os.makedirs(tmpdir + "/" + aport)
        with open(tmpdir + "/" + aport + "/PKGBUILD", 'w'):
            pass

    func = pmd.helpers.debports.guess_main
    assert func(args, "qemu-x86_64") == tmpdir + "/temp/qemu"
    assert func(args, "qemu-system-x86_64") == tmpdir + "/temp/qemu"
    assert func(args, "some-pkg-sub-pkg") == tmpdir + "/main/some-pkg"
    assert func(args, "qemuPackageWithoutDashes") is None


def test_guess_main_dev(args, tmpdir):
    # Fake debports folder
    tmpdir = str(tmpdir)
    args.debports = tmpdir
    os.makedirs(tmpdir + "/temp/plasma")
    with open(tmpdir + "/temp/plasma/PKGBUILD", 'w'):
        pass

    func = pmd.helpers.debports.guess_main_dev
    assert func(args, "plasma-framework-dev") is None
    assert func(args, "plasma-dev") == tmpdir + "/temp/plasma"

    func = pmd.helpers.debports.guess_main
    assert func(args, "plasma-framework-dev") is None
    assert func(args, "plasma-randomsubpkg") == tmpdir + "/temp/plasma"
