# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import glob
import os
import pytest
import shutil
import sys

import pmd_test  # noqa
import pmd_test.const
import pmd.build.newpkgbuild
import pmd.config
import pmd.config.init
import pmd.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import pmd.parse
    cfg = f"{pmd_test.const.testdata}/channels.cfg"
    sys.argv = ["pmdebootstrap.py", "--config-channels", cfg, "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_newpkgbuild(args, monkeypatch, tmpdir):
    testdata = pmd_test.const.testdata

    # Fake functions
    def confirm_true(*nargs):
        return True

    def confirm_false(*nargs):
        return False

    # Preparation
    monkeypatch.setattr(pmd.helpers.cli, "confirm", confirm_false)
    pmd.build.init(args)
    args.debports = tmpdir = str(tmpdir)
    shutil.copy(f"{testdata}/debports.cfg", args.debports)
    func = pmd.build.newpkgbuild

    # Show the help
    func(args, "main", ["-h"])
    assert glob.glob(f"{tmpdir}/*") == [f"{tmpdir}/debports.cfg"]

    # Test package
    pkgname = "testpackage"
    func(args, "main", [pkgname])
    pkgbuild_path = tmpdir + "/main/" + pkgname + "/PKGBUILD"
    pkgbuild = pmd.parse.pkgbuild(pkgbuild_path)
    assert pkgbuild["pkgname"] == pkgname
    assert pkgbuild["pkgdesc"] == ""

    # Don't overwrite
    with pytest.raises(RuntimeError) as e:
        func(args, "main", [pkgname])
    assert "Aborted" in str(e.value)

    # Overwrite
    monkeypatch.setattr(pmd.helpers.cli, "confirm", confirm_true)
    pkgdesc = "testdescription"
    func(args, "main", ["-d", pkgdesc, pkgname])
    pmd.helpers.other.cache["pkgbuild"] = {}
    pkgbuild = pmd.parse.pkgbuild(pkgbuild_path)
    assert pkgbuild["pkgname"] == pkgname
    assert pkgbuild["pkgdesc"] == pkgdesc

    # There should be no src folder
    assert not os.path.exists(tmpdir + "/main/" + pkgname + "/src")
