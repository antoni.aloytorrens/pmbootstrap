# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import pmd_test  # noqa
import pmd.chroot.apk_static
import pmd.config.debports
import pmd.parse.apkindex
import pmd.helpers.logging
import pmd.helpers.run
import pmd.parse.bootimg


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def pmdebootstrap_run(args, parameters, check=True):
    """Execute pmdebootstrap.py with a test pmdebootstrap.conf."""
    return pmd.helpers.run.user(args, ["./pmdebootstrap.py"] + parameters,
                                working_dir=pmd.config.pmd_src,
                                check=check)


def test_crossdirect_rust(args):
    """ Set up buildroot_armv7 chroot for building, but remove /usr/bin/rustc.
        Build hello-world-rust for armv7, to verify that it uses
        /native/usr/bin/rustc instead of /usr/bin/rustc. The package has a
        check() function, which makes sure that the built program is actually
        working. """
    pmdebootstrap_run(args, ["-y", "zap"])

    # Remember previously selected device
    cfg = pmd.config.load(args)
    old_device = cfg['pmdebootstrap']['device']

    try:
        # First, switch to device that is known to exist on all channels,
        # such as qemu-amd64. Currently selected device may not exist in
        # stable branch!
        cfg['pmdebootstrap']['device'] = 'qemu-amd64'
        pmd.config.save(args, cfg)

        # Switch to "v20.05" channel, as a stable release of debian is more
        # likely to have the same rustc version across various architectures.
        # If armv7/x86_64 have a different rustc version, this test will fail:
        # 'found crate `std` compiled by an incompatible version of rustc'
        pmd.config.debports.switch_to_channel_branch(args, "v21.03")

        pmdebootstrap_run(args, ["build_init", "-barmv7"])
        pmdebootstrap_run(args, ["chroot", "--add=rust", "-barmv7", "--",
                               "mv", "/usr/bin/rustc", "/usr/bin/rustc_"])
        pmdebootstrap_run(args, ["build", "hello-world-rust", "--arch=armv7",
                               "--force"])
        # Make /native/usr/bin/rustc unusuable too, to make the build fail
        pmdebootstrap_run(args, ["chroot", "--", "rm", "/usr/bin/rustc"])
        assert pmdebootstrap_run(args, ["build", "hello-world-rust",
                                      "--arch=armv7", "--force"],
                               check=False) == 1

        # Make /usr/bin/rustc usable again, to test fallback with qemu
        pmdebootstrap_run(args, ["chroot", "-barmv7", "--",
                               "mv", "/usr/bin/rustc_", "/usr/bin/rustc"])
        pmdebootstrap_run(args, ["build", "hello-world-rust", "--arch=armv7",
                               "--force"])
    finally:
        # Clean up
        pmd.config.debports.switch_to_channel_branch(args, "edge")
        pmdebootstrap_run(args, ["-y", "zap"])

        # Restore previously selected device
        cfg['pmdebootstrap']['device'] = old_device
        pmd.config.save(args, cfg)
