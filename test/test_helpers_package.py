# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import pmd_test  # noqa
import pmd.helpers.logging
import pmd.helpers.package


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap", "init"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_helpers_package_get_debports_and_cache(args, monkeypatch):
    """ Test pmd.helpers.package.get(): find in debports, use cached result """

    # Fake PKGBUILD data
    def stub(args, pkgname, must_exist):
        return {"arch": ["armv7"],
                "depends": ["testdepend"],
                "pkgname": "testpkgname",
                "provides": ["testprovide"],
                "options": [],
                "checkdepends": [],
                "subpackages": {},
                "makedepends": [],
                "pkgver": "1.0",
                "pkgrel": "1"}
    monkeypatch.setattr(pmd.helpers.debports, "get", stub)

    package = {"arch": ["armv7"],
               "depends": ["testdepend"],
               "pkgname": "testpkgname",
               "provides": ["testprovide"],
               "version": "1.0-r1"}
    func = pmd.helpers.package.get
    assert func(args, "testpkgname", "armv7") == package

    # Cached result
    monkeypatch.delattr(pmd.helpers.debports, "get")
    assert func(args, "testpkgname", "armv7") == package


def test_helpers_package_get_apkindex(args, monkeypatch):
    """ Test pmd.helpers.package.get(): find in apkindex """

    # Fake APKINDEX data
    fake_apkindex_data = {"arch": "armv7",
                          "depends": ["testdepend"],
                          "pkgname": "testpkgname",
                          "provides": ["testprovide"],
                          "version": "1.0-r1"}

    def stub(args, pkgname, arch, must_exist):
        if arch != fake_apkindex_data["arch"]:
            return None
        return fake_apkindex_data
    monkeypatch.setattr(pmd.parse.apkindex, "package", stub)

    # Given arch
    package = {"arch": ["armv7"],
               "depends": ["testdepend"],
               "pkgname": "testpkgname",
               "provides": ["testprovide"],
               "version": "1.0-r1"}
    func = pmd.helpers.package.get
    assert func(args, "testpkgname", "armv7") == package

    # Other arch
    assert func(args, "testpkgname", "x86_64") == package


def test_helpers_package_depends_recurse(args):
    """ Test pmd.helpers.package.depends_recurse() """

    # Put fake data into the pmd.helpers.package.get() cache
    cache = {"a": {False: {"pkgname": "a", "depends": ["b", "c"]}},
             "b": {False: {"pkgname": "b", "depends": []}},
             "c": {False: {"pkgname": "c", "depends": ["d"]}},
             "d": {False: {"pkgname": "d", "depends": ["b"]}}}
    pmd.helpers.other.cache["pmd.helpers.package.get"]["armhf"] = cache

    # Normal runs
    func = pmd.helpers.package.depends_recurse
    assert func(args, "a", "armhf") == ["a", "b", "c", "d"]
    assert func(args, "d", "armhf") == ["b", "d"]

    # Cached result
    pmd.helpers.other.cache["pmd.helpers.package.get"]["armhf"] = {}
    assert func(args, "d", "armhf") == ["b", "d"]


def test_helpers_package_check_arch_package(args):
    """ Test pmd.helpers.package.check_arch(): binary = True """
    # Put fake data into the pmd.helpers.package.get() cache
    func = pmd.helpers.package.check_arch
    cache = {"a": {False: {"arch": []}}}
    pmd.helpers.other.cache["pmd.helpers.package.get"]["armhf"] = cache

    cache["a"][False]["arch"] = ["all !armhf"]
    assert func(args, "a", "armhf") is False

    cache["a"][False]["arch"] = ["all"]
    assert func(args, "a", "armhf") is True

    cache["a"][False]["arch"] = ["noarch"]
    assert func(args, "a", "armhf") is True

    cache["a"][False]["arch"] = ["armhf"]
    assert func(args, "a", "armhf") is True

    cache["a"][False]["arch"] = ["aarch64"]
    assert func(args, "a", "armhf") is False


def test_helpers_package_check_arch_debports(args, monkeypatch):
    """ Test pmd.helpers.package.check_arch(): binary = False """
    func = pmd.helpers.package.check_arch
    fake_pmaport = {"arch": []}

    def fake_debports_get(args, pkgname, must_exist=False):
        return fake_pmaport
    monkeypatch.setattr(pmd.helpers.debports, "get", fake_debports_get)

    fake_pmaport["arch"] = ["armhf"]
    assert func(args, "a", "armhf", False) is True

    fake_pmaport["arch"] = ["all", "!armhf"]
    assert func(args, "a", "armhf", False) is False
