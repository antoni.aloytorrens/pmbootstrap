# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import sys
import time
import pytest

import pmd_test  # noqa
import pmd.helpers.git
import pmd.helpers.logging
import pmd.parse.version


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_file_is_older_than(args, tmpdir):
    # Create a file last modified 10s ago
    tempfile = str(tmpdir) + "/test"
    pmd.helpers.run.user(args, ["touch", tempfile])
    past = time.time() - 10
    os.utime(tempfile, (-1, past))

    # Check the bounds
    func = pmd.helpers.file.is_older_than
    assert func(tempfile, 9) is True
    assert func(tempfile, 10) is True
    assert func(tempfile, 11) is False
