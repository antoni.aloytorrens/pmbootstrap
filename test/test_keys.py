# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import sys
import pytest
import glob
import filecmp

import pmd_test  # noqa
import pmd.parse.apkindex
import pmd.helpers.logging
import pmd.config


@pytest.fixture
def args(request):
    import pmd.parse
    sys.argv = ["pmdebootstrap.py", "chroot"]
    args = pmd.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    pmd.helpers.logging.init(args)
    request.addfinalizer(pmd.helpers.logging.logfd.close)
    return args


def test_keys(args):
    # Get the debian-keys apk filename
    pmd.chroot.init(args)
    version = pmd.parse.apkindex.package(args, "debian-keys")["version"]
    pattern = (args.work + "/cache_apk_" + pmd.config.arch_native +
               "/debian-keys-" + version + ".*.apk")
    filename = os.path.basename(glob.glob(pattern)[0])

    # Extract it to a temporary folder
    temp = "/tmp/test_keys_extract"
    temp_outside = args.work + "/chroot_native" + temp
    if os.path.exists(temp_outside):
        pmd.chroot.root(args, ["rm", "-r", temp])
    pmd.chroot.user(args, ["mkdir", "-p", temp])
    pmd.chroot.user(args, ["tar", "xvf", "/var/cache/apk/" + filename],
                    working_dir=temp)

    # Get all relevant key file names as {"filename": "full_outside_path"}
    keys_upstream = {}
    for arch in pmd.config.build_device_architectures + ["x86_64"]:
        pattern = temp_outside + "/usr/share/apk/keys/" + arch + "/*.pub"
        for path in glob.glob(pattern):
            keys_upstream[os.path.basename(path)] = path
    assert len(keys_upstream)

    # Check if the keys are mirrored correctly
    mirror_path_keys = pmd.config.apk_keys_path
    for key, original_path in keys_upstream.items():
        mirror_path = mirror_path_keys + "/" + key
        assert filecmp.cmp(mirror_path, original_path, False)

    # Find postmarketOS keys
    keys_pmos = ["build.postmarketos.org.rsa.pub"]
    for key in keys_pmos:
        assert os.path.exists(mirror_path_keys + "/" + key)

    # Find outdated keys, which need to be removed
    glob_result = glob.glob(mirror_path_keys + "/*.pub")
    assert len(glob_result)
    for path in glob_result:
        key = os.path.basename(key)
        assert key in keys_pmos or key in keys_upstream
