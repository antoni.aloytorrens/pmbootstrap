# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import multiprocessing
import os
import pmd.parse.arch
import sys

#
# Exported functions
#
from pmd.config.load import load
from pmd.config.save import save
from pmd.config.merge_with_args import merge_with_args
from pmd.config.sudo import which_sudo


#
# Exported variables (internal configuration)
#
version = "1.46.0"
pmd_src = os.path.normpath(os.path.realpath(__file__) + "/../../..")
apk_keys_path = pmd_src + "/pmd/data/keys"
arch_native = pmd.parse.arch.debian_native()

# apk-tools minimum version
# https://pkgs.debianlinux.org/packages?name=apk-tools&branch=edge
# Update this frequently to prevent a MITM attack with an outdated version
# (which may contain a vulnerable apk/openssl, and allows an attacker to
# exploit the system!)
apk_tools_min_version = {"edge": "2.12.9-r6",
                         "v3.16": "2.12.9-r3",
                         "v3.15": "2.12.7-r3",
                         "v3.14": "2.12.7-r0",
                         "v3.13": "2.12.7-r0",
                         "v3.12": "2.10.8-r1"}

# postmarketOS debports compatibility (checked against "version" in debports.cfg)
debports_min_version = "7"

# Version of the work folder (as asked during 'pmdebootstrap init'). Increase
# this number, whenever migration is required and provide the migration code,
# see migrate_work_folder()).
work_version = 6

# Programs that pmdebootstrap expects to be available from the host system. Keep
# in sync with README.md, and try to keep the list as small as possible. The
# idea is to run almost everything in Debian chroots.
required_programs = ["git", "openssl", "ps", "pbuilder", "dpkg-sig", "reprepro"]
sudo = which_sudo()

# Keys saved in the config file (mostly what we ask in 'pmdebootstrap init')
config_keys = ["debports",
               "ccache_size",
               "device",
               "extra_packages",
               "hostname",
               "build_pkgs_on_install",
               "is_default_channel",
               "jobs",
               "kernel",
               "keymap",
               "locale",
               "mirror_debian",
               "mirrors_postmarketos",
               "nonfree_firmware",
               "nonfree_userland",
               "ssh_keys",
               "ssh_key_glob",
               "timezone",
               "ui",
               "ui_extras",
               "user",
               "work",
               "boot_size",
               "extra_space",
               "sudo_timer",
               "qemu_redir_stdio"]

# Config file/commandline default values
# $WORK gets replaced with the actual value for args.work (which may be
# overridden on the commandline)
defaults = {
    "debports": "$WORK/cache_git/debports",
    "ccache_size": "5G",
    "is_default_channel": True,
    "cipher": "aes-xts-plain64",
    "config": (os.environ.get('XDG_CONFIG_HOME') or
               os.path.expanduser("~/.config")) + "/pmdebootstrap.cfg",
    "device": "qemu-amd64",
    "extra_packages": "none",
    "fork_debian": False,
    "hostname": "",
    "build_pkgs_on_install": True,
    # A higher value is typically desired, but this can lead to VERY long open
    # times on slower devices due to host systems being MUCH faster than the
    # target device (see issue #429).
    "iter_time": "200",
    "jobs": str(multiprocessing.cpu_count() + 1),
    "kernel": "stable",
    "keymap": "",
    "locale": "C.UTF-8",
    "log": "$WORK/log.txt",
    "mirror_debian": "http://dl-cdn.debianlinux.org/debian/",
    # NOTE: mirrors_postmarketos variable type is supposed to be
    #       comma-separated string, not a python list or any other type!
    "mirrors_postmarketos": "http://mirror.postmarketos.org/postmarketos/",
    "nonfree_firmware": True,
    "nonfree_userland": False,
    "port_distccd": "33632",
    "ssh_keys": False,
    "ssh_key_glob": "~/.ssh/id_*.pub",
    "timezone": "GMT",
    "ui": "weston",
    "ui_extras": False,
    "user": "user",
    "work": os.path.expanduser("~") + "/.local/var/pmdebootstrap",
    "boot_size": "256",
    "extra_space": "0",
    "sudo_timer": False,
    "qemu_redir_stdio": False
}


# Whether we're connected to a TTY (which allows things like e.g. printing
# progress bars)
is_interactive = sys.stdout.isatty() and \
    sys.stderr.isatty() and \
    sys.stdin.isatty()


# ANSI escape codes to highlight stdout
styles = {
    "BLUE": '\033[94m',
    "BOLD": '\033[1m',
    "GREEN": '\033[92m',
    "RED": '\033[91m',
    "YELLOW": '\033[93m',
    "END": '\033[0m'
}

if "NO_COLOR" in os.environ:
    for style in styles.keys():
        styles[style] = ""


# List of available locales taken from musl-locales package; see
# https://pkgs.debianlinux.org/contents?name=musl-locales
locales = [
    "C.UTF-8",
    "ch_DE.UTF-8",
    "de_CH.UTF-8",
    "de_DE.UTF-8",
    "en_GB.UTF-8",
    "en_US.UTF-8",
    "es_ES.UTF-8",
    "fr_FR.UTF-8",
    "it_IT.UTF-8",
    "nb_NO.UTF-8",
    "nl_NL.UTF-8",
    "pt_BR.UTF-8",
    "ru_RU.UTF-8",
    "sv_SE.UTF-8"
]

# Legacy channels and their new names (pmd#2015)
# wheezy needs kernel option (see mini-buildd documentation)
debports_channels_legacy = {"buster": "10",
                            "stretch": "9",
                            "jessie":  "8",
                            "wheezy":  "7"}
#
# CHROOT
#

# Usually the ID for the first user created is 1000. However, we want
# pmdebootstrap to work even if the 'user' account inside the chroots has
# another UID, so we force it to be different.
chroot_uid_user = "12345"

# The PATH variable used inside all chroots
chroot_path = ":".join([
    "/usr/lib/ccache/bin",
    "/usr/local/sbin",
    "/usr/local/bin",
    "/usr/sbin:/usr/bin",
    "/sbin",
    "/bin"
])

# The PATH variable used on the host, to find the "chroot" and "sh"
# executables. As pmdebootstrap runs as user, not as root, the location
# for the chroot executable may not be in the PATH (Debian).
chroot_host_path = os.environ["PATH"] + ":/usr/sbin/"

# Folders that get mounted inside the chroot
# $WORK gets replaced with args.work
# $ARCH gets replaced with the chroot architecture (eg. x86_64, armhf)
# $CHANNEL gets replaced with the release channel (e.g. edge, v21.03)
chroot_mount_bind = {
    "/proc": "/proc",
    "$WORK/cache_apk_$ARCH": "/var/cache/apk",
    "$WORK/cache_ccache_$ARCH": "/mnt/pmdebootstrap-ccache",
    "$WORK/cache_distfiles": "/var/cache/distfiles",
    "$WORK/cache_git": "/mnt/pmdebootstrap-git",
    "$WORK/config_abuild": "/mnt/pmdebootstrap-abuild-config",
    "$WORK/config_apk_keys": "/etc/apk/keys",
    "$WORK/packages/$CHANNEL": "/mnt/pmdebootstrap-packages",
}

# Building chroots (all chroots, except for the rootfs_ chroot) get symlinks in
# the "pmos" user's home folder pointing to mountfolders from above.
# Rust packaging is new and still a bit weird in Debian and postmarketOS. As of
# writing, we only have one package (squeekboard), and use cargo to download
# the source of all dependencies at build time and compile it. Usually, this is
# a no-go, but at least until this is resolved properly, let's cache the
# dependencies and downloads as suggested in "Caching the Cargo home in CI":
# https://doc.rust-lang.org/cargo/guide/cargo-home.html
chroot_home_symlinks = {
    "/mnt/pmdebootstrap-abuild-config": "/home/pmos/.abuild",
    "/mnt/pmdebootstrap-ccache": "/home/pmos/.ccache",
    "/mnt/pmdebootstrap-packages": "/home/pmos/packages/pmos",
}

# Device nodes to be created in each chroot. Syntax for each entry:
# [permissions, type, major, minor, name]
chroot_device_nodes = [
    [666, "c", 1, 3, "null"],
    [666, "c", 1, 5, "zero"],
    [666, "c", 1, 7, "full"],
    [644, "c", 1, 8, "random"],
    [644, "c", 1, 9, "urandom"],
]

# Age in hours that we keep the APKINDEXes before downloading them again.
# You can force-update them with 'pmdebootstrap update'.
apkindex_retention_time = 4


# When chroot is considered outdated (in seconds)
chroot_outdated = 3600 * 24 * 2

#
# BUILD
#
# Officially supported host/target architectures for postmarketOS. Only
# specify architectures supported by Debian here. For cross-compiling,
# we need to generate the "musl-$ARCH", "binutils-$ARCH" and "gcc-$ARCH"
# packages (use "pmdebootstrap aportgen musl-armhf" etc.).
build_device_architectures = ["armhf", "armel", "arm64", "amd64", "i386"]

# Packages that will be installed in a chroot before it builds packages
# for the first time
build_packages = ["dpkg-dev", "dpkg-sig", "devscripts", "equivs", "reprepro", "build-essential", "ccache", "git", "makedeb"]

#
# PARSE (UTIL FOR MAKEDEB)
#
# Variables belonging to a package or subpackage in makedebbuild files
makedebbuild_package_attributes = {
    "pkgdesc": {},
    "depends": {"array": True},
    "provides": {"array": True},
    "provider_priority": {"int": True},
    "install": {"array": True},

    # UI meta-packages can specify apps in "_pmd_recommends" to be explicitly
    # installed by default, and not implicitly as dependency of the UI meta-
    # package ("depends"). This makes these apps uninstallable, without
    # removing the meta-package. (#1933). To disable this feature, use:
    # "pmdebootstrap install --no-recommends".
    "_pmd_recommends": {"array": True},

    # UI meta-packages can specify groups to which the user must be added
    # to access specific hardware such as LED indicators.
    "_pmd_groups": {"array": True},

    # postmarketos-base, UI and device packages can use _pmd_select to provide
    # additional configuration options in "pmdebootstrap init" that allow
    # selecting alternative providers for a virtual APK package.
    "_pmd_select": {"array": True},
}

# Variables in makedebbuild files that get parsed
makedebbuild_attributes = {
    **makedebbuild_package_attributes,

    "arch": {"array": True},
    "depends_dev": {"array": True},
    "makedepends": {"array": True},
    "checkdepends": {"array": True},
    "options": {"array": True},
    "triggers": {"array": True},
    "pkgname": {},
    "pkgrel": {},
    "pkgver": {},
    "subpackages": {},
    "url": {},

    # cross-compilers
    "makedepends_build": {"array": True},
    "makedepends_host": {"array": True},

    # kernels
    "_flavor": {},
    "_device": {},
    "_kernver": {},
    "_outdir": {},
    "_config": {},

    # mesa
    "_llvmver": {},

    # Overridden packages
    "_pkgver": {},
    "_pkgname": {},

    # git commit
    "_commit": {},
    "source": {"array": True},
}

#
# GIT
#
git_repos = {
    "debports_upstream": "https://gitlab.com/aat-debian-repository/debports.git",
    "debports": "https://gitlab.com/aat-debian-repository/debports.git",
}

# When a git repository is considered outdated (in seconds)
# (Measuring timestamp of FETCH_HEAD: https://stackoverflow.com/a/9229377)
git_repo_outdated = 3600 * 24 * 2

#
# SIDELOAD
#
sideload_sudo_prompt = "[sudo] password for %u@%h: "
