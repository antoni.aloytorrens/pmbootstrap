# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pmd.config


def merge_with_args(args):
    """
    We have the internal config (pmd/config/__init__.py) and the user config
    (usually ~/.config/pmdebootstrap.cfg, can be changed with the '-c'
    parameter).

    Args holds the variables parsed from the commandline (e.g. -j fills out
    args.jobs), and values specified on the commandline count the most.

    In case it is not specified on the commandline, for the keys in
    pmd.config.config_keys, we look into the value set in the the user config.

    When that is empty as well (e.g. just before pmdebootstrap init), or the key
    is not in pmd.config_keys, we use the default value from the internal
    config.
    """
    # Use defaults from the user's config file
    cfg = pmd.config.load(args)
    for key in cfg["pmdebootstrap"]:
        if key not in args or getattr(args, key) is None:
            value = cfg["pmdebootstrap"][key]
            if key in pmd.config.defaults:
                default = pmd.config.defaults[key]
                if isinstance(default, bool):
                    value = (value.lower() == "true")
            setattr(args, key, value)
    setattr(args, 'selected_providers', cfg['providers'])

    # Use defaults from pmd.config.defaults
    for key, value in pmd.config.defaults.items():
        if key not in args or getattr(args, key) is None:
            setattr(args, key, value)
