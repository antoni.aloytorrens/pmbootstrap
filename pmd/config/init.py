# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import glob
import json
import os
import shutil

import pmd.config
import pmd.config.debports
import pmd.helpers.cli
import pmd.helpers.http
import pmd.helpers.logging
import pmd.helpers.other
import pmd.helpers.debports
import pmd.helpers.run
import pmd.chroot.zap
import pmd.parse._pkgbuild


def require_programs():
    missing = []
    for program in pmd.config.required_programs:
        if not shutil.which(program):
            missing.append(program)
    if missing:
        raise RuntimeError("Can't find all programs required to run"
                           " pmdebootstrap. Please install first:"
                           f" {', '.join(missing)}")


def ask_for_work_path(args):
    """
    Ask for the work path, until we can create it (when it does not exist) and
    write into it.
    :returns: (path, exists)
              * path: is the full path, with expanded ~ sign
              * exists: is False when the folder did not exist before we tested
                        whether we can create it
    """
    logging.info("Location of the 'work' path. Multiple chroots"
                 " of your choice will be created"
                 " in there.")
    while True:
        try:
            work = os.path.expanduser(pmd.helpers.cli.ask(
                "Work path", None, args.work, False))
            work = os.path.realpath(work)
            exists = os.path.exists(work)

            # Work must not be inside the pmdebootstrap path
            if (work == pmd.config.pmd_src or
                    work.startswith(f"{pmd.config.pmd_src}/")):
                logging.fatal("ERROR: The work path must not be inside the"
                              " pmdebootstrap path. Please specify another"
                              " location.")
                continue

            # Create the folder with a version file
            if not exists:
                os.makedirs(work, 0o700, True)

            # If the version file doesn't exists yet because we either just
            # created the work directory or the user has deleted it for
            # whatever reason then we need to write initialize it.
            work_version_file = f"{work}/version"
            if not os.path.isfile(work_version_file):
                with open(work_version_file, "w") as handle:
                    handle.write(f"{pmd.config.work_version}\n")

            # Create cache_git dir, so it is owned by the host system's user
            # (otherwise pmd.helpers.mount.bind would create it as root)
            os.makedirs(f"{work}/cache_git", 0o700, True)
            return (work, exists)
        except OSError:
            logging.fatal("ERROR: Could not create this folder, or write"
                          " inside it! Please try again.")


def ask_for_channel(args):
    """ Ask for the postmarketOS release channel. The channel dictates, which
        debports branch pmdebootstrap will check out, and which repository URLs
        will be used when initializing chroots.
        :returns: channel name (e.g. "edge", "v21.03") """
    channels_cfg = pmd.helpers.git.parse_channels_cfg(args)
    count = len(channels_cfg["channels"])

    # List channels
    logging.info("Choose the postmarketOS release channel.")
    logging.info(f"Available ({count}):")
    for channel, channel_data in channels_cfg["channels"].items():
        logging.info(f"* {channel}: {channel_data['description']}")

    # Default for first run: "recommended" from channels.cfg
    # Otherwise, if valid: channel from debports.cfg of current branch
    # The actual channel name is not saved in pmdebootstrap.cfg, because then we
    # would need to sync it with what is checked out in debports.git.
    default = pmd.config.debports.read_config(args)["channel"]
    choices = channels_cfg["channels"].keys()
    if args.is_default_channel or default not in choices:
        default = channels_cfg["meta"]["recommended"]

    # Ask until user gives valid channel
    while True:
        ret = pmd.helpers.cli.ask("Channel", None, default,
                                  complete=choices)
        if ret in choices:
            return ret
        logging.fatal("ERROR: Invalid channel specified, please type in one"
                      " from the list above.")


def frontend(args):
    require_programs()

    # Work folder (needs to be first, so we can create chroots early)
    cfg = pmd.config.load(args)
    work, work_exists = ask_for_work_path(args)
    cfg["pmdebootstrap"]["work"] = work

    # Update args and save config (so chroots and 'pmdebootstrap log' work)
    pmd.helpers.args.update_work(args, work)
    pmd.config.save(args, cfg)

    # Migrate work dir if necessary
    pmd.helpers.other.migrate_work_folder(args)

    # Clone debports
    pmd.config.debports.init(args)

    # Choose release channel, possibly switch debports branch
    channel = ask_for_channel(args)
    pmd.config.debports.switch_to_channel_branch(args, channel)
    cfg["pmdebootstrap"]["is_default_channel"] = "False"

    # debports path (if users change it with: 'pmdebootstrap --debports=... init')
    cfg["pmdebootstrap"]["debports"] = args.debports

    # Save config
    pmd.config.save(args, cfg)

    # Zap existing chroots
    if (work_exists and
            len(glob.glob(args.work + "/chroot_*")) and
            pmd.helpers.cli.confirm(
                args, "Zap existing chroots to apply configuration?",
                default=True)):
        setattr(args, "deviceinfo", info)

        # Do not zap any existing packages or cache_http directories
        pmd.chroot.zap(args, confirm=False)

    logging.info("WARNING: The chroots and git repositories in the work dir do"
                 " not get updated automatically.")
    logging.info("Run 'pmdebootstrap status' once a day before working with"
                 " pmdebootstrap to make sure that everything is up-to-date.")
    logging.info("DONE!")
