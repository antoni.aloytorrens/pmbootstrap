# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import configparser
import os
import pmd.config


def load(args):
    cfg = configparser.ConfigParser()
    if os.path.isfile(args.config):
        cfg.read(args.config)

    if "pmdebootstrap" not in cfg:
        cfg["pmdebootstrap"] = {}
    if "providers" not in cfg:
        cfg["providers"] = {}

    for key in pmd.config.defaults:
        if key in pmd.config.config_keys and key not in cfg["pmdebootstrap"]:
            cfg["pmdebootstrap"][key] = str(pmd.config.defaults[key])

        # We used to save default values in the config, which can *not* be
        # configured in "pmdebootstrap init". That doesn't make sense, we always
        # want to use the defaults from pmd/config/__init__.py in that case,
        # not some outdated version we saved some time back (eg. debports folder,
        # postmarketOS binary packages mirror).
        if key not in pmd.config.config_keys and key in cfg["pmdebootstrap"]:
            logging.debug("Ignored unconfigurable and possibly outdated"
                          " default value from config:"
                          f" {cfg['pmdebootstrap'][key]}")
            del cfg["pmdebootstrap"][key]

    return cfg
