# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import configparser
import logging
import os

import pmd.config
import pmd.helpers.git
import pmd.helpers.debports


def check_legacy_folder():
    # Existing pmdebootstrap/debports must be a symlink
    link = pmd.config.pmd_src + "/debports"
    if os.path.exists(link) and not os.path.islink(link):
        raise RuntimeError("The path '" + link + "' should be a"
                           " symlink pointing to the new debports"
                           " repository, which was split from the"
                           " pmdebootstrap repository (#383). Consider"
                           " making a backup of that folder, then delete"
                           " it and run 'pmdebootstrap init' again to let"
                           " pmdebootstrap clone the debports repository and"
                           " set up the symlink.")


def clone(args):
    logging.info("Setting up the native chroot and cloning the package build"
                 " recipes (debports)...")

    # Set up the native chroot and clone debports
    pmd.helpers.git.clone(args, "debports")


def symlink(args):
    # Create the symlink
    # This won't work when pmdebootstrap was installed system wide, but that's
    # okay since the symlink is only intended to make the migration to the
    # debports repository easier.
    link = pmd.config.pmd_src + "/debports"
    try:
        os.symlink(args.debports, link)
        logging.info("NOTE: debports path: " + link)
    except:
        logging.info("NOTE: debports path: " + args.debports)


def check_version_debports(real):
    # Compare versions
    min = pmd.config.debports_min_version
    if pmd.parse.version.compare(real, min) >= 0:
        return

    # Outated error
    logging.info("NOTE: your debports folder has version " + real + ", but" +
                 " version " + min + " is required.")
    raise RuntimeError("Run 'pmdebootstrap pull' to update your debports.")


def check_version_pmdebootstrap(min):
    # Compare versions
    real = pmd.config.version
    if pmd.parse.version.compare(real, min) >= 0:
        return

    # Show versions
    logging.info("NOTE: you are using pmdebootstrap version " + real + ", but" +
                 " version " + min + " is required.")

    # Error for git clone
    pmd_src = pmd.config.pmd_src
    if os.path.exists(pmd_src + "/.git"):
        raise RuntimeError("Please update your local pmdebootstrap repository."
                           " Usually with: 'git -C \"" + pmd_src + "\" pull'")

    # Error for package manager installation
    raise RuntimeError("Please update your pmdebootstrap version (with your"
                       " distribution's package manager, or with pip, "
                       " depending on how you have installed it). If that is"
                       " not possible, consider cloning the latest version"
                       " of pmdebootstrap from git.")


def read_config(args):
    """ Read and verify debports.cfg. """
    # Try cache first
    cache_key = "pmd.config.debports.read_config"
    if pmd.helpers.other.cache[cache_key]:
        return pmd.helpers.other.cache[cache_key]

    # Require the config
    path_cfg = args.debports + "/debports.cfg"
    if not os.path.exists(path_cfg):
        raise RuntimeError("Invalid debports repository, could not find the"
                           " config: " + path_cfg + ". Have you executed pmdebootstrap init?")

    # Load the config
    cfg = configparser.ConfigParser()
    cfg.read(path_cfg)
    ret = cfg["debports"]

    # Version checks
    check_version_debports(ret["version"])
    check_version_pmdebootstrap(ret["pmdebootstrap_min_version"])

    # Translate legacy channel names
    ret["channel"] = pmd.helpers.debports.get_channel_new(ret["channel"])

    # Cache and return
    pmd.helpers.other.cache[cache_key] = ret
    return ret


def read_config_channel(args):
    """ Get the properties of the currently active channel in debports.git,
        as specified in channels.cfg (https://postmarketos.org/channels.cfg).
        :returns: {"description: ...,
                   "branch_debports": ...,
                   "branch_debports": ...,
                   "mirrordir_debian": ...} """
    channel = read_config(args)["channel"]
    channels_cfg = pmd.helpers.git.parse_channels_cfg(args)

    if channel in channels_cfg["channels"]:
        return channels_cfg["channels"][channel]

    # Channel not in channels.cfg, try to be helpful
    branch = pmd.helpers.git.rev_parse(args, args.debports,
                                       extra_args=["--abbrev-ref"])
    branches_official = pmd.helpers.git.get_branches_official(args, "debports")
    branches_official = ", ".join(branches_official)
    remote = pmd.helpers.git.get_upstream_remote(args, "debports")
    logging.info("NOTE: fix the error by rebasing or cherry picking relevant"
                 " commits from this branch onto a branch that is on a"
                 f" supported channel: {branches_official}")
    logging.info("NOTE: as workaround, you may pass --config-channels with a"
                 " custom channels.cfg. Reference:"
                 " https://postmarketos.org/channels.cfg")
    raise RuntimeError(f"Current branch '{branch}' of debports.git is on"
                       f" channel '{channel}', but this channel was not"
                       f" found in channels.cfg (of {remote}/master"
                       " branch). Looks like a very old branch.")


def init(args):
    check_legacy_folder()
    if not os.path.exists(args.debports):
        clone(args)
    symlink(args)
    read_config(args)


def switch_to_channel_branch(args, channel_new):
    """ Checkout the channel's branch in debports.git.
        :channel_new: channel name (e.g. "edge", "v21.03")
        :returns: True if another branch was checked out, False otherwise """
    # Check current debports branch channel
    channel_current = read_config(args)["channel"]
    if channel_current == channel_new:
        return False

    # List current and new branches/channels
    channels_cfg = pmd.helpers.git.parse_channels_cfg(args)
    branch_new = channels_cfg["channels"][channel_new]["branch_debports"]
    branch_current = pmd.helpers.git.rev_parse(args, args.debports,
                                               extra_args=["--abbrev-ref"])
    logging.info(f"Currently checked out branch '{branch_current}' of"
                 f" debports.git is on channel '{channel_current}'.")
    logging.info(f"Switching to branch '{branch_new}' on channel"
                 f" '{channel_new}'...")

    # Make sure we don't have mounts related to the old channel
    pmd.chroot.shutdown(args)

    # Attempt to switch branch (git gives a nice error message, mentioning
    # which files need to be committed/stashed, so just pass it through)
    if pmd.helpers.run.user(args, ["git", "checkout", branch_new],
                            args.debports, "interactive", check=False):
        raise RuntimeError("Failed to switch branch. Go to your debports and"
                           " fix what git complained about, then try again: "
                           f"{args.debports}")

    # Invalidate all caches
    pmd.helpers.other.init_cache()

    # Verify debports.cfg on new branch
    read_config(args)
    return True
