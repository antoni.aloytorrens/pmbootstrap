# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import argparse
import copy
import os

try:
    import argcomplete
except ImportError:
    argcomplete = False

import pmd.config
import pmd.parse.arch
import pmd.helpers.args
import pmd.helpers.debports

""" This file is about parsing command line arguments passed to pmdebootstrap, as
    well as generating the help pages (pmdebootstrap -h). All this is done with
    Python's argparse. The parsed arguments get extended and finally stored in
    the "args" variable, which is prominently passed to most functions all
    over the pmdebootstrap code base.

    See pmd/helpers/args.py for more information about the args variable. """


def toggle_other_boolean_flags(*other_destinations, value=True):
    """ Helper function to group several argparse flags to one. Sets multiple
        other_destination to value.

        :param other_destinations: 'the other argument names' str
        :param value 'the value to set the other_destinations to' bool
        :returns custom Action"""

    class SetOtherDestinationsAction(argparse.Action):
        def __init__(self, option_strings, dest, **kwargs):
            super().__init__(option_strings, dest, nargs=0, const=value,
                             default=value, **kwargs)

        def __call__(self, parser, namespace, values, option_string=None):
            for destination in other_destinations:
                setattr(namespace, destination, value)

    return SetOtherDestinationsAction

# TODO: export = export repository
def arguments_export(subparser):
    pass

def arguments_pkgrel_bump(subparser):
    ret = subparser.add_parser("pkgrel_bump", help="increase the pkgrel to"
                               " indicate that a package must be rebuilt"
                               " because of a dependency change")
    ret.add_argument("--dry", action="store_true", help="instead of modifying"
                     " PKGBUILDs, exit with >0 when a package would have been"
                     " bumped")

    # Mutually exclusive: "--auto" or package names
    mode = ret.add_mutually_exclusive_group(required=True)
    mode.add_argument("--auto", action="store_true", help="all packages which"
                      " depend on a library which had an incompatible update"
                      " (libraries with a soname bump)")
    mode.add_argument("packages", nargs="*", default=[])
    return ret

# TODO: For makedeb
def arguments_newmakedebbuild(subparser):
    """
    Wrapper for Debian's "makedebbuild" command.

    Most parameters will get directly passed through, and they are defined in
    "pmd/config/__init__.py". That way they can be used here and when passing
    them through in "pmd/helpers/frontend.py". The order of the parameters is
    kept the same as in "newmakedebbuild -h".
    """
    sub = subparser.add_parser("newmakedebbuild", help="get a template to package"
                               " new software")
    sub.add_argument("--folder", help="set postmarketOS debports folder"
                     " (default: main)", default="main")

    """
    # Passthrough: Strings (e.g. -d "my description")
    for entry in pmd.config.makedebbuild_arguments_strings:
        sub.add_argument(entry[0], dest=entry[1], help=entry[2])

    # Passthrough: Package type switches (e.g. -C for CMake)
    group = sub.add_mutually_exclusive_group()
    for entry in pmd.config.makedebbuild_arguments_switches_pkgtypes:
        group.add_argument(entry[0], dest=entry[1], help=entry[2],
                           action="store_true")

    # Passthrough: Other switches (e.g. -c for copying sample files)
    for entry in pmd.config.makedebbuild_arguments_switches_other:
        sub.add_argument(entry[0], dest=entry[1], help=entry[2],
                         action="store_true")

    # Force switch
    sub.add_argument("-f", dest="force", action="store_true",
                     help="force even if directory already exists")

    # Passthrough: PKGNAME[-PKGVER] | SRCURL
    sub.add_argument("pkgname_pkgver_srcurl",
                     metavar="PKGNAME[-PKGVER] | SRCURL",
                     help="set either the package name (optionally with the"
                     " PKGVER at the end, e.g. 'hello-world-1.0') or the"
                     " download link to the source archive")
    """


def arguments_aportupgrade(subparser):
    ret = subparser.add_parser("aportupgrade", help="check for outdated"
                               " packages that need upgrading")
    ret.add_argument("--dry", action="store_true", help="instead of modifying"
                     " PKGBUILDs, print the changes that would be made")
    ret.add_argument("--ref", help="git ref (tag, commit, etc) to use")

    # Mutually exclusive: "--all" or package names
    mode = ret.add_mutually_exclusive_group(required=True)
    mode.add_argument("--all", action="store_true", help="iterate through all"
                      " packages")
    mode.add_argument("--all-stable", action="store_true", help="iterate"
                      " through all non-git packages")
    mode.add_argument("--all-git", action="store_true", help="iterate through"
                      " all git packages")
    mode.add_argument("packages", nargs="*", default=[])
    return ret


def arguments_repo_missing(subparser):
    ret = subparser.add_parser("repo_missing")
    package = ret.add_argument("package", nargs="?", help="only look at a"
                               " specific package and its dependencies")
    if argcomplete:
        package.completer = package_completer
    ret.add_argument("--arch", choices=pmd.config.build_device_architectures,
                     default=pmd.config.arch_native)
    ret.add_argument("--built", action="store_true",
                     help="include packages which exist in the binary repos")
    ret.add_argument("--overview", action="store_true",
                     help="only print the pkgnames without any details")
    return ret


def arguments_lint(subparser):
    lint = subparser.add_parser("lint", help="run quality checks on debports"
                                             " (required to pass CI)")
    add_packages_arg(lint, nargs="*")


def arguments_status(subparser):
    ret = subparser.add_parser("status",
                               help="quick health check for the work dir")
    ret.add_argument("--details", action="store_true",
                     help="list passing checks in detail, not as summary")
    return ret


def package_completer(prefix, action, parser=None, parsed_args=None):
    args = parsed_args
    pmd.config.merge_with_args(args)
    pmd.helpers.args.replace_placeholders(args)
    pmd.helpers.other.init_cache()
    packages = set(
        package for package in pmd.helpers.debports.get_list(args)
        if package.startswith(prefix))
    return packages


def add_packages_arg(subparser, name="packages", *args, **kwargs):
    arg = subparser.add_argument(name, *args, **kwargs)
    if argcomplete:
        arg.completer = package_completer


def arguments():
    parser = argparse.ArgumentParser(prog="pmdebootstrap")
    arch_native = pmd.config.arch_native
    arch_choices = set(pmd.config.build_device_architectures + [arch_native])
    mirrors_pmos_default = pmd.config.defaults["mirrors_postmarketos"]

    # Other
    parser.add_argument("-V", "--version", action="version",
                        version=pmd.config.version)
    parser.add_argument("-c", "--config", dest="config",
                        default=pmd.config.defaults["config"],
                        help="path to pmdebootstrap.cfg file (default in"
                             " ~/.config/)")
    parser.add_argument("--config-channels",
                        help="path to channels.cfg (which is by default"
                             " read from debports.git, origin/master branch)")
    parser.add_argument("-mp", "--mirror-pmOS", dest="mirrors_postmarketos",
                        help="postmarketOS mirror, disable with: -mp='',"
                             " specify multiple with: -mp='one' -mp='two',"
                             f" default: {mirrors_pmos_default}",
                        metavar="URL", action="append", default=[])
    parser.add_argument("-m", "--mirror-debian", dest="mirror_debian",
                        help="Debian Linux mirror, default: " +
                             pmd.config.defaults["mirror_debian"],
                        metavar="URL")
    parser.add_argument("-d", "--debports",
                        help="postmarketos debports (debports) path")
    parser.add_argument("-t", "--timeout", help="seconds after which processes"
                        " get killed that stopped writing any output (default:"
                        " 900)", default=900, type=float)
    parser.add_argument("-w", "--work", help="folder where all data"
                        " gets stored (chroots, caches, built packages)")
    parser.add_argument("-y", "--assume-yes", help="Assume 'yes' to all"
                        " question prompts. WARNING: this option will"
                        " cause normal 'are you sure?' prompts to be"
                        " disabled!",
                        action="store_true")
    parser.add_argument("--as-root", help="Allow running as root (not"
                        " recommended, may screw up your work folders"
                        " directory permissions!)", dest="as_root",
                        action="store_true")
    parser.add_argument("-o", "--offline", help="Do not attempt to update"
                        " the package index files", action="store_true")

    # Logging
    parser.add_argument("-l", "--log", dest="log", default=None,
                        help="path to log file")
    parser.add_argument("--details-to-stdout", dest="details_to_stdout",
                        help="print details (e.g. build output) to stdout,"
                             " instead of writing to the log",
                        action="store_true")
    parser.add_argument("-v", "--verbose", dest="verbose",
                        action="store_true", help="write even more to the"
                        " logfiles (this may reduce performance)")
    parser.add_argument("-q", "--quiet", dest="quiet", action="store_true",
                        help="do not output any log messages")

    # Actions
    sub = parser.add_subparsers(title="action", dest="action")
    sub.add_parser("init", help="initialize config file")
    sub.add_parser("shutdown", help="umount, unregister binfmt")
    sub.add_parser("index", help="re-index all repositories with custom built"
                   " packages (do this after manually removing package files)")
    sub.add_parser("work_migrate", help="run this before using pmdebootstrap"
                                        " non-interactively to migrate the"
                                        " work folder version on demand")
    arguments_repo_missing(sub)
    arguments_export(sub)
    arguments_pkgrel_bump(sub)
    arguments_aportupgrade(sub)
    arguments_lint(sub)
    arguments_status(sub)

    # For makedeb
    arguments_newmakedebbuild(sub)

    # Action: log
    log = sub.add_parser("log", help="follow the pmdebootstrap logfile")
    log_distccd = sub.add_parser(
        "log_distccd",
        help="follow the distccd logfile")
    for action in [log, log_distccd]:
        action.add_argument("-n", "--lines", default="60",
                            help="count of initial output lines")
        action.add_argument("-c", "--clear", help="clear the log",
                            action="store_true", dest="clear_log")

    # Action: zap
    zap = sub.add_parser("zap", help="safely delete chroot folders")
    zap.add_argument("--dry", action="store_true", help="instead of actually"
                     " deleting anything, print out what would have been"
                     " deleted")
    zap.add_argument("-hc", "--http", action="store_true", help="also delete"
                     " http cache")
    zap.add_argument("-d", "--distfiles", action="store_true", help="also"
                     " delete downloaded source tarballs")
    zap.add_argument("-p", "--pkgs-local", action="store_true",
                     dest="pkgs_local",
                     help="also delete *all* locally compiled packages")
    zap.add_argument("-m", "--pkgs-local-mismatch", action="store_true",
                     dest="pkgs_local_mismatch",
                     help="also delete locally compiled packages without"
                     " existing aport of same version")
    zap.add_argument("-o", "--pkgs-online-mismatch", action="store_true",
                     dest="pkgs_online_mismatch",
                     help="also delete outdated packages from online mirrors"
                     " (that have been downloaded to the apk cache)")

    zap_all_delete_args = ["http", "distfiles", "pkgs_local",
                           "pkgs_local_mismatch", "netboot", "pkgs_online_mismatch",
                           "rust"]
    zap_all_delete_args_print = [arg.replace("_", "-")
                                 for arg in zap_all_delete_args]
    zap.add_argument("-a", "--all",
                     action=toggle_other_boolean_flags(*zap_all_delete_args),
                     help="delete everything, equivalent to: "
                     f"--{' --'.join(zap_all_delete_args_print)}")

    # Action: stats
    stats = sub.add_parser("stats", help="show ccache stats")
    stats.add_argument("--arch", default=arch_native, choices=arch_choices)

    # Action: update
    update = sub.add_parser("update", help="update all existing APKINDEX"
                            " files")
    update.add_argument("--arch", default=None, choices=arch_choices,
                        help="only update a specific architecture")
    update.add_argument("--non-existing", action="store_true", help="do not"
                        " only update the existing APKINDEX files, but all of"
                        " them", dest="non_existing")

    # Action: build_init / chroot
    build_init = sub.add_parser("build_init", help="initialize build"
                                " environment (usually you do not need to call"
                                " this)")
    chroot = sub.add_parser("chroot", help="start shell in chroot")
    chroot.add_argument("--add", help="build/install comma separated list of"
                        " packages in the chroot before entering it")
    chroot.add_argument("--user", help="run the command as user, not as root",
                        action="store_true")
    chroot.add_argument("--output", choices=["log", "stdout", "interactive",
                        "tui", "background"], help="how the output of the"
                        " program should be handled, choose from: 'log',"
                        " 'stdout', 'interactive', 'tui' (default),"
                        " 'background'. Details: pmd/helpers/run_core.py",
                        default="tui")
    chroot.add_argument("command", default=["sh", "-i"], help="command"
                        " to execute inside the chroot. default: sh",
                        nargs='*')
    chroot.add_argument("-x", "--xauth", action="store_true",
                        help="Copy .Xauthority and set environment variables,"
                             " so X11 applications can be started (native"
                             " chroot only)")
    chroot.add_argument("-i", "--install-blockdev", action="store_true",
                        help="Create a sparse image file and mount it as"
                              " /dev/install, just like during the"
                              " installation process.")
    for action in [build_init, chroot]:
        suffix = action.add_mutually_exclusive_group()
        if action == chroot:
            suffix.add_argument("-r", "--rootfs", action="store_true",
                                help="Chroot for the device root file system")
        suffix.add_argument("-b", "--buildroot", nargs="?", const="device",
                            choices=arch_choices,
                            help="Chroot for building packages")
        suffix.add_argument("-s", "--suffix", default=None,
                            help="Specify any chroot suffix, defaults to"
                                 " 'native'")

    # Action: install
    """
    aportgen.add_argument("--fork-debian", help="fork the debian upstream"
                          " package", action="store_true",
                          dest="fork_debian")
    add_packages_arg(aportgen, nargs="+")
    """

    # Action: build
    build = sub.add_parser("build", help="create a package for a"
                           " specific architecture")
    build.add_argument("--arch", choices=arch_choices, default=None,
                       help="CPU architecture to build for (default: " +
                       arch_native + " or first available architecture in"
                       " PKGBUILD)")
    build.add_argument("--force", action="store_true", help="even build if not"
                       " necessary")
    build.add_argument("--strict", action="store_true", help="(slower) zap and"
                       " install only required depends when building, to"
                       " detect dependency errors")
    build.add_argument("--src", help="override source used to build the"
                       " package with a local folder (the PKGBUILD must"
                       " expect the source to be in $builddir, so you might"
                       " need to adjust it)",
                       nargs=1)
    build.add_argument("-i", "--ignore-depends", action="store_true",
                       help="only build and install makedepends from an"
                       " PKGBUILD, ignore the depends (old behavior). This is"
                       " faster for device packages for example, because then"
                       " you don't need to build and install the kernel. But"
                       " it is incompatible with how Debian's abuild handles"
                       " it.",
                       dest="ignore_depends")
    build.add_argument("-n", "--no-depends", action="store_true",
                       help="never build dependencies, abort instead",
                       dest="no_depends")
    build.add_argument("--envkernel", action="store_true",
                       help="Create an apk package from the build output of"
                       " a kernel compiled with envkernel.sh.")
    add_packages_arg(build, nargs="+")

    # Action: pkgbuild_parse
    pkgbuild_parse = sub.add_parser("pkgbuild_parse")
    add_packages_arg(pkgbuild_parse, nargs="*")

    # Action: apkindex_parse
    apkindex_parse = sub.add_parser("apkindex_parse")
    apkindex_parse.add_argument("apkindex_path")
    add_packages_arg(apkindex_parse, "package", nargs="?")

    # Action: config
    config = sub.add_parser("config",
                            help="get and set pmdebootstrap options")
    config.add_argument("-r", "--reset", action="store_true",
                        help="Reset config options with the given name to it's"
                        " default.")
    config.add_argument("name", nargs="?", help="variable name, one of: " +
                        ", ".join(sorted(pmd.config.config_keys)),
                        choices=pmd.config.config_keys, metavar="name")
    config.add_argument("value", nargs="?", help="set variable to value")

    # Action: pull
    sub.add_parser("pull", help="update all git repositories that pmdebootstrap"
                   " cloned (debports, etc.)")

    if argcomplete:
        argcomplete.autocomplete(parser, always_complete_options="long")

    # Parse and extend arguments (also backup unmodified result from argparse)
    args = parser.parse_args()
    setattr(args, "from_argparse", copy.deepcopy(args))
    setattr(args.from_argparse, "from_argparse", args.from_argparse)
    pmd.helpers.args.init(args)
    return args
