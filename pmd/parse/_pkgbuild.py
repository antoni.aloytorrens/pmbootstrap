# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import os
import re
from collections import OrderedDict

import pmd.config
import pmd.parse.version

# sh variable name regex: https://stackoverflow.com/a/2821201/3527128

# ${foo}
revar = re.compile(r"\${([a-zA-Z_]+[a-zA-Z0-9_]*)}")

# $foo
revar2 = re.compile(r"\$([a-zA-Z_]+[a-zA-Z0-9_]*)")

# ${var/foo/bar}, ${var/foo/}, ${var/foo} -- replace foo with bar
revar3 = re.compile(r"\${([a-zA-Z_]+[a-zA-Z0-9_]*)/([^/]+)(?:/([^/]*?))?}")

# ${foo#bar} -- cut off bar from foo from start of string
revar4 = re.compile(r"\${([a-zA-Z_]+[a-zA-Z0-9_]*)#(.*)}")


def replace_variable(pkgbuild, value: str) -> str:
    def log_key_not_found(match):
        logging.verbose(f"{pkgbuild['pkgname']}: key '{match.group(1)}' for"
                        f" replacing '{match.group(0)}' not found, ignoring")

    # ${foo}
    for match in revar.finditer(value):
        try:
            logging.verbose("{}: replace '{}' with '{}'".format(
                            pkgbuild["pkgname"], match.group(0),
                            pkgbuild[match.group(1)]))
            value = value.replace(match.group(0), pkgbuild[match.group(1)], 1)
        except KeyError:
            log_key_not_found(match)

    # $foo
    for match in revar2.finditer(value):
        try:
            newvalue = pkgbuild[match.group(1)]
            logging.verbose("{}: replace '{}' with '{}'".format(
                            pkgbuild["pkgname"], match.group(0),
                            newvalue))
            value = value.replace(match.group(0), newvalue, 1)
        except KeyError:
            log_key_not_found(match)

    # ${var/foo/bar}, ${var/foo/}, ${var/foo}
    for match in revar3.finditer(value):
        try:
            newvalue = pkgbuild[match.group(1)]
            search = match.group(2)
            replacement = match.group(3)
            if replacement is None:  # arg 3 is optional
                replacement = ""
            newvalue = newvalue.replace(search, replacement, 1)
            logging.verbose("{}: replace '{}' with '{}'".format(
                            pkgbuild["pkgname"], match.group(0), newvalue))
            value = value.replace(match.group(0), newvalue, 1)
        except KeyError:
            log_key_not_found(match)

    # ${foo#bar}
    rematch4 = revar4.finditer(value)
    for match in rematch4:
        try:
            newvalue = pkgbuild[match.group(1)]
            substr = match.group(2)
            if newvalue.startswith(substr):
                newvalue = newvalue.replace(substr, "", 1)
            logging.verbose("{}: replace '{}' with '{}'".format(
                            pkgbuild["pkgname"], match.group(0), newvalue))
            value = value.replace(match.group(0), newvalue, 1)
        except KeyError:
            log_key_not_found(match)

    return value


def function_body(path, func):
    """
    Get the body of a function in an PKGBUILD.

    :param path: full path to the PKGBUILD
    :param func: name of function to get the body of.
    :returns: function body in an array of strings.
    """
    func_body = []
    in_func = False
    lines = read_file(path)
    for line in lines:
        if in_func:
            if line.startswith("}"):
                in_func = False
                break
            func_body.append(line)
            continue
        else:
            if line.startswith(func + "() {"):
                in_func = True
                continue
    return func_body


def read_file(path):
    """
    Read an PKGBUILD file

    :param path: full path to the PKGBUILD
    :returns: contents of an PKGBUILD as a list of strings
    """
    with open(path, encoding="utf-8") as handle:
        lines = handle.readlines()
        if handle.newlines != '\n':
            raise RuntimeError(f"Wrong line endings in PKGBUILD: {path}")
    return lines


def parse_attribute(attribute, lines, i, path):
    """
    Parse one attribute from the PKGBUILD.

    It may be written across multiple lines, use a quoting sign and/or have
    a comment at the end. Some examples:

    pkgrel=3
    options="!check" # ignore this comment
    arch='all !armhf'
    depends="
        first-pkg
        second-pkg"

    :param attribute: from the PKGBUILD, i.e. "pkgname"
    :param lines: \n-terminated list of lines from the PKGBUILD
    :param i: index of the line we are currently looking at
    :param path: full path to the PKGBUILD (for error message)
    :returns: (found, value, i)
              found: True if the attribute was found in line i, False otherwise
              value: that was parsed from the line
              i: line that was parsed last
    """
    # Check for and cut off "attribute="
    if not lines[i].startswith(attribute + "="):
        return (False, None, i)
    value = lines[i][len(attribute + "="):-1]

    # Determine end quote sign
    end_char = None
    for char in ["'", "\""]:
        if value.startswith(char):
            end_char = char
            value = value[1:]
            break

    # Single line
    if not end_char:
        value = value.split("#")[0].rstrip()
        return (True, value, i)
    if end_char in value:
        value = value.split(end_char, 1)[0]
        return (True, value, i)

    # Parse lines until reaching end quote
    i += 1
    while i < len(lines):
        line = lines[i]
        value += " "
        if end_char in line:
            value += line.split(end_char, 1)[0].strip()
            return (True, value.strip(), i)
        value += line.strip()
        i += 1

    raise RuntimeError(f"Can't find closing quote sign ({end_char}) for"
                       f" attribute '{attribute}' in: {path}")


def _parse_attributes(path, lines, pkgbuild_attributes, ret):
    """
    Parse attributes from a list of lines. Variables are replaced with values
    from ret (if found) and split into the format configured in
    pkgbuild_attributes.

    :param lines: the lines to parse
    :param pkgbuild_attributes: the attributes to parse
    :param ret: a dict to update with new parsed variable
    """
    for i in range(len(lines)):
        for attribute, options in pkgbuild_attributes.items():
            found, value, i = parse_attribute(attribute, lines, i, path)
            if not found:
                continue

            ret[attribute] = replace_variable(ret, value)

    if "subpackages" in pkgbuild_attributes:
        subpackages = OrderedDict()
        for subpkg in ret["subpackages"].split(" "):
            if subpkg:
                _parse_subpackage(path, lines, ret, subpackages, subpkg)
        ret["subpackages"] = subpackages

    # Split attributes
    for attribute, options in pkgbuild_attributes.items():
        if options.get("array", False):
            # Split up arrays, delete empty strings inside the list
            ret[attribute] = list(filter(None, ret[attribute].split(" ")))
        if options.get("int", False):
            if ret[attribute]:
                ret[attribute] = int(ret[attribute])
            else:
                ret[attribute] = 0


def _parse_subpackage(path, lines, pkgbuild, subpackages, subpkg):
    """
    Attempt to parse attributes from a subpackage function.
    This will attempt to locate the subpackage function in the PKGBUILD and
    update the given attributes with values set in the subpackage function.

    :param path: path to PKGBUILD
    :param lines: the lines to parse
    :param pkgbuild: dict of attributes already parsed from PKGBUILD
    :param subpackages: the subpackages dict to update
    :param subpkg: the subpackage to parse
                   (may contain subpackage function name separated by :)
    """
    subpkgparts = subpkg.split(":")
    subpkgname = subpkgparts[0]
    subpkgsplit = subpkgname[subpkgname.rfind("-") + 1:]
    if len(subpkgparts) > 1:
        subpkgsplit = subpkgparts[1]

    # Find start and end of package function
    start = end = 0
    prefix = subpkgsplit + "() {"
    for i in range(len(lines)):
        if lines[i].startswith(prefix):
            start = i + 1
        elif start and lines[i].startswith("}"):
            end = i
            break

    if not start:
        # Unable to find subpackage function in the PKGBUILD.
        # The subpackage function could be actually missing, or this is a
        # problem in the parser. For now we also don't handle subpackages with
        # default functions (e.g. -dev or -doc).
        # In the future we may want to specifically handle these, and throw
        # an exception here for all other missing subpackage functions.
        subpackages[subpkgname] = None
        logging.verbose(
            f"{pkgbuild['pkgname']}: subpackage function '{subpkgsplit}' for "
            f"subpackage '{subpkgname}' not found, ignoring")
        return

    if not end:
        raise RuntimeError(
            f"Could not find end of subpackage function, no line starts with "
            f"'}}' after '{prefix}' in {path}")

    lines = lines[start:end]
    # Strip tabs before lines in function
    lines = [line.strip() + "\n" for line in lines]

    # Copy variables
    pkgbuild = pkgbuild.copy()
    pkgbuild["subpkgname"] = subpkgname

    # Parse relevant attributes for the subpackage
    _parse_attributes(
        path, lines, pmd.config.pkgbuild_package_attributes, pkgbuild)

    # Return only properties interesting for subpackages
    ret = {}
    for key in pmd.config.pkgbuild_package_attributes:
        ret[key] = pkgbuild[key]
    subpackages[subpkgname] = ret


def pkgbuild(path, check_pkgver=True, check_pkgname=True):
    """
    Parse relevant information out of the PKGBUILD file. This is not meant
    to be perfect and catch every edge case (for that, a full shell parser
    would be necessary!). Instead, it should just work with the use-cases
    covered by pmdebootstrap and not take too long.
    Run 'pmdebootstrap pkgbuild_parse hello-world' for a full output example.

    :param path: full path to the PKGBUILD
    :param check_pkgver: verify that the pkgver is valid.
    :param check_pkgname: the pkgname must match the name of the aport folder
    :returns: relevant variables from the PKGBUILD. Arrays get returned as
              arrays.
    """
    # Try to get a cached result first (we assume that the debports don't change
    # in one pmdebootstrap call)
    if path in pmd.helpers.other.cache["pkgbuild"]:
        return pmd.helpers.other.cache["pkgbuild"][path]

    # Read the file and check line endings
    lines = read_file(path)

    # Parse all attributes from the config
    ret = {key: "" for key in pmd.config.pkgbuild_attributes.keys()}
    _parse_attributes(path, lines, pmd.config.pkgbuild_attributes, ret)

    # Sanity check: pkgname
    suffix = f"/{ret['pkgname']}/PKGBUILD"
    if check_pkgname:
        if not os.path.realpath(path).endswith(suffix):
            logging.info(f"Folder: '{os.path.dirname(path)}'")
            logging.info(f"Pkgname: '{ret['pkgname']}'")
            raise RuntimeError("The pkgname must be equal to the name of"
                               " the folder that contains the PKGBUILD!")

    # Sanity check: pkgver
    if check_pkgver:
        if ("-r" in ret["pkgver"] or not
           pmd.parse.version.validate(ret["pkgver"])):
            logging.info(
                "NOTE: Valid pkgvers are described here: "
                "https://wiki.debianlinux.org/wiki/PKGBUILD_Reference#pkgver")
            raise RuntimeError(f"Invalid pkgver '{ret['pkgver']}' in"
                               f" PKGBUILD: {path}")

    # Fill cache
    pmd.helpers.other.cache["pkgbuild"][path] = ret
    return ret


def kernels(args, device):
    """
    Get the possible kernels from a device-* PKGBUILD.

    :param device: the device name, e.g. "lg-mako"
    :returns: None when the kernel is hardcoded in depends
    :returns: kernel types and their description (as read from the subpackages)
              possible types: "downstream", "stable", "mainline"
              example: {"mainline": "Mainline description",
                        "downstream": "Downstream description"}
    """
    # Read the PKGBUILD
    pkgbuild_path = pmd.helpers.devices.find_path(args, device, 'PKGBUILD')
    if pkgbuild_path is None:
        return None
    subpackages = pkgbuild(pkgbuild_path)["subpackages"]

    # Read kernels from subpackages
    ret = {}
    subpackage_prefix = f"device-{device}-kernel-"
    for subpkgname, subpkg in subpackages.items():
        if not subpkgname.startswith(subpackage_prefix):
            continue
        if subpkg is None:
            raise RuntimeError(
                f"Cannot find subpackage function for: {subpkgname}")
        name = subpkgname[len(subpackage_prefix):]
        ret[name] = subpkg["pkgdesc"]

    # Return
    if ret:
        return ret
    return None


def _parse_comment_tags(lines, tag):
    """
    Parse tags defined as comments in a PKGBUILD file. This can be used to
    parse e.g. the maintainers of a package (defined using # Maintainer:).

    :param lines: lines of the PKGBUILD
    :param tag: the tag to parse, e.g. Maintainer
    :returns: array of values of the tag, one per line
    """
    prefix = f'# {tag}:'
    ret = []
    for line in lines:
        if line.startswith(prefix):
            ret.append(line[len(prefix):].strip())
    return ret


def maintainers(path):
    """
    Parse maintainers of an PKGBUILD file. They should be defined using
    # Maintainer: (first maintainer) and # Co-Maintainer: (additional
    maintainers).

    :param path: full path to the PKGBUILD
    :returns: array of (at least one) maintainer, or None
    """
    lines = read_file(path)
    maintainers = _parse_comment_tags(lines, 'Maintainer')
    if not maintainers:
        return None

    # An PKGBUILD should only have one Maintainer:,
    # in debports others should be defined using Co-Maintainer:
    if len(maintainers) > 1:
        raise RuntimeError("Multiple Maintainer: lines in PKGBUILD")

    maintainers += _parse_comment_tags(lines, 'Co-Maintainer')
    if '' in maintainers:
        raise RuntimeError("Empty (Co-)Maintainer: tag")
    return maintainers


def unmaintained(path):
    """
    Return if (and why) an PKGBUILD might be unmaintained. This should be
    defined using a # Unmaintained: <reason> tag in the PKGBUILD.

    :param path: full path to the PKGBUILD
    :returns: reason why PKGBUILD is unmaintained, or None
    """
    unmaintained = _parse_comment_tags(read_file(path), 'Unmaintained')
    if not unmaintained:
        return None
    return '\n'.join(unmaintained)
