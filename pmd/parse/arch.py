# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import fnmatch
import platform
import pmd.parse.arch

def debian_native():
    machine = platform.machine()

    mapping = {
        "i686": "i386",
        "x86_64": "amd64",
        "aarch64": "arm64",
        "armv7l": "armhf",
        "armv6l": "armel"
    }
    if machine in mapping:
        return mapping[machine]
    raise ValueError("Can not map platform.machine '" + machine + "'"
                     " to the right Debian Linux architecture")


def from_chroot_suffix(args, suffix):
    if suffix == "native":
        return pmd.config.arch_native
    if suffix in [f"rootfs_{args.device}", f"installer_{args.device}"]:
        return args.deviceinfo["arch"]
    if suffix.startswith("buildroot_"):
        return suffix.split("_", 1)[1]

    raise ValueError("Invalid chroot suffix: " + suffix +
                     " (wrong device chosen in 'init' step?)")


def debian_to_qemu(arch):
    """
    Convert the architecture to the string used in the QEMU packaging.
    """

    mapping = {
        "i686": "i386",
        "x86_64": "amd64",
        "aarch64": "arm64",
        "armv7l": "armhf",
        "armv6l": "armel"
    }
    for pattern, arch_qemu in mapping.items():
        if fnmatch.fnmatch(arch, pattern):
            return arch_qemu
    raise ValueError("Can not map Debian architecture '" + arch + "'"
                     " to the right Debian architecture.")


def debian_to_kernel(arch):
    """
    Convert the architecture to the string used inside the kernel sources.
    You can read the mapping from the linux-vanilla PKGBUILD for example.
    """
    mapping = {
        "aarch64*": "arm64",
        "arm*": "arm",
        "ppc*": "powerpc",
        "s390*": "s390"
    }
    from IPython import embed; embed()
    for pattern, arch_kernel in mapping.items():
        if fnmatch.fnmatch(arch, pattern):
            return arch_kernel
    return arch


def debian_to_hostspec(arch):
    """
    See: abuild source code/functions.sh.in: arch_to_hostspec()
    """
    mapping = {
        "aarch64": "aarch64-debian-linux-musl",
        "armel": "armv5-debian-linux-musleabi",
        "armhf": "armv6-debian-linux-musleabihf",
        "armv7": "armv7-debian-linux-musleabihf",
        "mips": "mips-debian-linux-musl",
        "mips64": "mips64-debian-linux-musl",
        "mipsel": "mipsel-debian-linux-musl",
        "mips64el": "mips64el-debian-linux-musl",
        "ppc": "powerpc-debian-linux-musl",
        "ppc64": "powerpc64-debian-linux-musl",
        "ppc64le": "powerpc64le-debian-linux-musl",
        "s390x": "s390x-debian-linux-musl",
        "x86": "i586-debian-linux-musl",
        "x86_64": "x86_64-debian-linux-musl",
    }
    if arch in mapping:
        return mapping[arch]

    raise ValueError("Can not map Debian architecture '" + arch + "'"
                     " to the right hostspec value")


def cpu_emulation_required(arch):
    # Obvious case: host arch is target arch
    if pmd.config.arch_native == arch:
        return False

    # Other cases: host arch on the left, target archs on the right
    not_required = {
        "x86_64": ["x86"],
        "armv7": ["armel", "armhf"],
        "aarch64": ["armel", "armhf", "armv7"],
    }
    if pmd.config.arch_native in not_required:
        if arch in not_required[pmd.config.arch_native]:
            return False

    # No match: then it's required
    return True
