import glob
import logging
import os

import pmd.helpers.run
import pmd.helpers.frontend
import pmd.export


def frontend(args):
    # Create the export folder
    target = args.export_folder
    if not os.path.exists(target):
        pmd.helpers.run.user(args, ["mkdir", "-p", target])

    # Rootfs image note
    chroot = args.work + "/chroot_native"
    pattern = chroot + "/home/pmos/rootfs/" + args.device + "*.img"
    if not glob.glob(pattern):
        logging.info("NOTE: To export the rootfs image, run 'pmdebootstrap"
                     " install' first (without the 'sdcard' parameter).")

    # Do the export, print all files
    logging.info("Export symlinks to: " + target)
    if args.odin_flashable_tar:
        pmd.export.odin(args, flavor, target)
    pmd.export.symlinks(args, flavor, target)
