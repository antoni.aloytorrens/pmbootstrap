# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import glob
import os
import logging
import pmd.chroot.user
import pmd.helpers.cli
import pmd.parse


def newpkgbuild(args, folder, args_passed, force=False):
    # Initialize build environment and build folder
    pmd.build.init(args)
    build = "/home/pmos/build"
    build_outside = args.work + "/chroot_native" + build
    if os.path.exists(build_outside):
        pmd.chroot.root(args, ["rm", "-r", build])
    pmd.chroot.user(args, ["mkdir", "-p", build])

    # Run newpkgbuild
    pmd.chroot.user(args, ["newpkgbuild"] + args_passed, working_dir=build)
    glob_result = glob.glob(build_outside + "/*/PKGBUILD")
    if not len(glob_result):
        return

    # Paths for copying
    source_pkgbuild = glob_result[0]
    pkgname = pmd.parse.pkgbuild(source_pkgbuild, False)["pkgname"]
    target = args.debports + "/" + folder + "/" + pkgname

    # Move /home/pmos/build/$pkgname/* to /home/pmos/build/*
    for path in glob.glob(build_outside + "/*/*"):
        path_inside = build + "/" + pkgname + "/" + os.path.basename(path)
        pmd.chroot.user(args, ["mv", path_inside, build])
    pmd.chroot.user(args, ["rmdir", build + "/" + pkgname])

    # Overwrite confirmation
    if os.path.exists(target):
        logging.warning("WARNING: Folder already exists: " + target)
        question = "Continue and delete its contents?"
        if not force and not pmd.helpers.cli.confirm(args, question):
            raise RuntimeError("Aborted.")
        pmd.helpers.run.user(args, ["rm", "-r", target])

    # Copy the aport (without the extracted src folder)
    logging.info("Create " + target)
    pmd.helpers.run.user(args, ["mkdir", "-p", target])
    for path in glob.glob(build_outside + "/*"):
        if not os.path.isdir(path):
            pmd.helpers.run.user(args, ["cp", path, target])
