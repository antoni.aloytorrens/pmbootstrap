# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import os

import pmd.config
import pmd.chroot.apk
import pmd.helpers.debports
import pmd.parse.arch


def arch(args, pkgname):
    """
    Find a good default in case the user did not specify for which architecture
    a package should be built.

    :returns: arch string like "x86_64" or "armhf". Preferred order, depending
              on what is supported by the PKGBUILD:
              * native arch
              * device arch
              * first arch in the PKGBUILD
    """
    aport = pmd.helpers.debports.find(args, pkgname)

    pkgbuild = pmd.parse.pkgbuild(f"{aport}/PKGBUILD")
    arches = pkgbuild["arch"]
    if ("noarch" in arches or
            "all" in arches or
            pmd.config.arch_native in arches):
        return pmd.config.arch_native

    try:
        return pkgbuild["arch"][0]
    except IndexError:
        return None


def suffix(pkgbuild, arch):
    if arch == pmd.config.arch_native:
        return "native"

    if "pmd:cross-native" in pkgbuild["options"]:
        return "native"

    return "buildroot_" + arch


def crosscompile(args, pkgbuild, arch, suffix):
    """
        :returns: None, "native", "crossdirect" or "distcc"
    """
    if not args.cross:
        return None
    if not pmd.parse.arch.cpu_emulation_required(arch):
        return None
    if suffix == "native":
        return "native"
    if args.no_crossdirect or "!pmd:crossdirect" in pkgbuild["options"]:
        return "distcc"
    return "crossdirect"
