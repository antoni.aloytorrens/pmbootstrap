# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import logging

import pmd.build
import pmd.chroot
import pmd.chroot.apk
import pmd.chroot.other
import pmd.helpers.debports
import pmd.helpers.run
import pmd.parse


def get_arch(pkgbuild):
    """
    Take the architecture from the PKGBUILD or complain if it's ambiguous. This
    function only gets called if --arch is not set.

    :param pkgbuild: looks like: {"pkgname": "linux-...",
                                  "arch": ["x86_64", "armhf", "aarch64"]}
                     or: {"pkgname": "linux-...", "arch": ["armhf"]}
    """
    pkgname = pkgbuild["pkgname"]

    # Disabled package (arch="")
    if not pkgbuild["arch"]:
        raise RuntimeError(f"'{pkgname}' is disabled (arch=\"\"). Please use"
                           " '--arch' to specify the desired architecture.")

    # Multiple architectures
    if len(pkgbuild["arch"]) > 1:
        raise RuntimeError(f"'{pkgname}' supports multiple architectures"
                           f" ({', '.join(pkgbuild['arch'])}). Please use"
                           " '--arch' to specify the desired architecture.")

    return pkgbuild["arch"][0]


def get_outputdir(args, pkgname, pkgbuild):
    """
    Get the folder for the kernel compilation output.
    For most PKGBUILDs, this is $builddir. But some older ones still use
    $srcdir/build (see the discussion in #1551).
    """
    # Old style ($srcdir/build)
    ret = "/home/pmos/build/src/build"
    chroot = args.work + "/chroot_native"
    if os.path.exists(chroot + ret + "/.config"):
        logging.warning("*****")
        logging.warning("NOTE: The code in this linux PKGBUILD is pretty old."
                        " Consider making a backup and migrating to a modern"
                        " version with: pmdebootstrap aportgen " + pkgname)
        logging.warning("*****")

        return ret

    # New style ($builddir)
    cmd = "srcdir=/home/pmos/build/src source PKGBUILD; echo $builddir"
    ret = pmd.chroot.user(args, ["sh", "-c", cmd],
                          "native", "/home/pmos/build",
                          output_return=True).rstrip()
    if os.path.exists(chroot + ret + "/.config"):
        return ret
    # Some Mediatek kernels use a 'kernel' subdirectory
    if os.path.exists(chroot + ret + "/kernel/.config"):
        return os.path.join(ret, "kernel")

    # Out-of-tree builds ($_outdir)
    if os.path.exists(chroot + ret + "/" + pkgbuild["_outdir"] + "/.config"):
        return os.path.join(ret, pkgbuild["_outdir"])

    # Not found
    raise RuntimeError("Could not find the kernel config. Consider making a"
                       " backup of your PKGBUILD and recreating it from the"
                       " template with: pmdebootstrap aportgen " + pkgname)


def menuconfig(args, pkgname, use_oldconfig):
    # Pkgname: allow omitting "linux-" prefix
    if not pkgname.startswith("linux-"):
        pkgname = "linux-" + pkgname

    # Read pkgbuild
    aport = pmd.helpers.debports.find(args, pkgname)
    pkgbuild = pmd.parse.pkgbuild(f"{aport}/PKGBUILD")
    arch = args.arch or get_arch(pkgbuild)
    suffix = pmd.build.autodetect.suffix(pkgbuild, arch)
    cross = pmd.build.autodetect.crosscompile(args, pkgbuild, arch, suffix)
    hostspec = pmd.parse.arch.debian_to_hostspec(arch)

    # Set up build tools and makedepends
    pmd.build.init(args, suffix)
    if cross:
        pmd.build.init_compiler(args, [], cross, arch)

    depends = pkgbuild["makedepends"]
    copy_xauth = False

    if use_oldconfig:
        kopt = "oldconfig"
    else:
        kopt = "menuconfig"
        if args.xconfig:
            depends += ["qt5-qtbase-dev", "font-noto"]
            kopt = "xconfig"
            copy_xauth = True
        elif args.nconfig:
            kopt = "nconfig"
            depends += ["ncurses-dev"]
        else:
            depends += ["ncurses-dev"]

    pmd.chroot.apk.install(args, depends)

    # Copy host's .xauthority into native
    if copy_xauth:
        pmd.chroot.other.copy_xauthority(args)

    # Patch and extract sources
    pmd.build.copy_to_buildpath(args, pkgname)
    logging.info("(native) extract kernel source")
    pmd.chroot.user(args, ["abuild", "unpack"], "native", "/home/pmos/build")
    logging.info("(native) apply patches")
    pmd.chroot.user(args, ["abuild", "prepare"], "native",
                    "/home/pmos/build", output="interactive",
                    env={"CARCH": arch})

    # Run make menuconfig
    outputdir = get_outputdir(args, pkgname, pkgbuild)
    logging.info("(native) make " + kopt)
    env = {"ARCH": pmd.parse.arch.debian_to_kernel(arch),
           "DISPLAY": os.environ.get("DISPLAY"),
           "XAUTHORITY": "/home/pmos/.Xauthority"}
    if cross:
        env["CROSS_COMPILE"] = f"{hostspec}-"
        env["CC"] = f"{hostspec}-gcc"
    pmd.chroot.user(args, ["make", kopt], "native",
                    outputdir, output="tui", env=env)

    # Find the updated config
    source = args.work + "/chroot_native" + outputdir + "/.config"
    if not os.path.exists(source):
        raise RuntimeError("No kernel config generated: " + source)

    # Update the aport (config and checksum)
    logging.info("Copy kernel config back to aport-folder")
    config = "config-" + pkgbuild["_flavor"] + "." + arch
    target = aport + "/" + config
    pmd.helpers.run.user(args, ["cp", source, target])
    pmd.build.checksum.update(args, pkgname)

    # Check config
    pmd.parse.kconfig.check(args, pkgbuild["_flavor"], force_anbox_check=False,
                            force_nftables_check=False,
                            force_containers_check=False,
                            force_zram_check=False,
                            force_netboot_check=False,
                            force_uefi_check=False,
                            details=True)
