# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import datetime
import logging
import os

import pmd.build
import pmd.chroot
import pmd.chroot.apk
import pmd.chroot.distccd
import pmd.helpers.debports
import pmd.helpers.repo
import pmd.parse
import pmd.parse.arch


def skip_already_built(pkgname, arch):
    """
    Check if the package was already built in this session, and add it
    to the cache in case it was not built yet.

    :returns: True when it can be skipped or False
    """
    if arch not in pmd.helpers.other.cache["built"]:
        pmd.helpers.other.cache["built"][arch] = []
    if pkgname in pmd.helpers.other.cache["built"][arch]:
        logging.verbose(pkgname + ": already checked this session,"
                        " no need to build it or its dependencies")
        return True
    pmd.helpers.other.cache["built"][arch].append(pkgname)
    return False


def get_pkgbuild(args, pkgname, arch):
    """
    Parse the PKGBUILD path for pkgname. When there is none, try to find it in
    the binary package APKINDEX files or raise an exception.

    :param pkgname: package name to be built, as specified in the PKGBUILD
    :returns: None or parsed PKGBUILD
    """
    # Get existing binary package indexes
    pmd.helpers.repo.update(args, arch)

    # Get pmaport, skip upstream only packages
    pmaport = pmd.helpers.debports.get(args, pkgname, False)
    if pmaport:
        return pmaport
    if pmd.parse.apkindex.providers(args, pkgname, arch, False):
        return None
    raise RuntimeError("Package '" + pkgname + "': Could not find aport, and"
                       " could not find this package in any APKINDEX!")


def check_build_for_arch(args, pkgname, arch):
    """
    Check if pmaport can be built or exists as binary for a specific arch.
    :returns: * True when it can be built
              * False when it can't be built, but exists in a binary repo
                (e.g. temp/mesa can't be built for x86_64, but Debian has it)
    :raises: RuntimeError if the package can't be built for the given arch and
             does not exist as binary package.
    """
    # Check for pmaport with arch
    if pmd.helpers.package.check_arch(args, pkgname, arch, False):
        return True

    # Check for binary package
    binary = pmd.parse.apkindex.package(args, pkgname, arch, False)
    if binary:
        pmaport = pmd.helpers.debports.get(args, pkgname)
        pmaport_version = pmaport["pkgver"] + "-r" + pmaport["pkgrel"]
        logging.debug(pkgname + ": found pmaport (" + pmaport_version + ") and"
                      " binary package (" + binary["version"] + ", from"
                      " postmarketOS or Debian), but pmaport can't be built"
                      " for " + arch + " -> using binary package")
        return False

    # No binary package exists and can't build it
    logging.info("NOTE: You can edit the 'arch=' line inside the PKGBUILD")
    if args.action == "build":
        logging.info("NOTE: Alternatively, use --arch to build for another"
                     " architecture ('pmdebootstrap build --arch=armhf " +
                     pkgname + "')")
    raise RuntimeError("Can't build '" + pkgname + "' for architecture " +
                       arch)


def get_depends(args, pkgbuild):
    """
    Debian's abuild always builds/installs the "depends" and "makedepends"
    of a package before building it. We used to only care about "makedepends"
    and it's still possible to ignore the depends with --ignore-depends.

    :returns: list of dependency pkgnames (eg. ["sdl2", "sdl2_net"])
    """
    # Read makedepends and depends
    ret = list(pkgbuild["makedepends"])
    if "!check" not in pkgbuild["options"]:
        ret += pkgbuild["checkdepends"]
    if "ignore_depends" not in args or not args.ignore_depends:
        ret += pkgbuild["depends"]
    ret = sorted(set(ret))

    # Don't recurse forever when a package depends on itself (#948)
    for pkgname in ([pkgbuild["pkgname"]] +
                    list(pkgbuild["subpackages"].keys())):
        if pkgname in ret:
            logging.verbose(pkgbuild["pkgname"] + ": ignoring dependency on"
                            " itself: " + pkgname)
            ret.remove(pkgname)
    return ret


def build_depends(args, pkgbuild, arch, strict):
    """
    Get and build dependencies with verbose logging messages.

    :returns: (depends, depends_built)
    """
    # Get dependencies
    pkgname = pkgbuild["pkgname"]
    depends = get_depends(args, pkgbuild)
    logging.verbose(pkgname + ": build/install dependencies: " +
                    ", ".join(depends))

    # --no-depends: check for binary packages
    depends_built = []
    if "no_depends" in args and args.no_depends:
        pmd.helpers.repo.update(args, arch)
        for depend in depends:
            # Ignore conflicting dependencies
            if depend.startswith("!"):
                continue
            # Check if binary package is missing
            if not pmd.parse.apkindex.package(args, depend, arch, False):
                raise RuntimeError("Missing binary package for dependency '" +
                                   depend + "' of '" + pkgname + "', but"
                                   " pmdebootstrap won't build any depends since"
                                   " it was started with --no-depends.")
            # Check if binary package is outdated
            pkgbuild_dep = get_pkgbuild(args, depend, arch)
            if pkgbuild_dep and \
               pmd.build.is_necessary(args, arch, pkgbuild_dep):
                raise RuntimeError(f"Binary package for dependency '{depend}'"
                                   f" of '{pkgname}' is outdated, but"
                                   f" pmdebootstrap won't build any depends"
                                   f" since it was started with --no-depends.")
    else:
        # Build the dependencies
        for depend in depends:
            if depend.startswith("!"):
                continue
            if package(args, depend, arch, strict=strict):
                depends_built += [depend]
        logging.verbose(pkgname + ": build dependencies: done, built: " +
                        ", ".join(depends_built))

    return (depends, depends_built)


def is_necessary_warn_depends(args, pkgbuild, arch, force, depends_built):
    """
    Check if a build is necessary, and warn if it is not, but there were
    dependencies built.

    :returns: True or False
    """
    pkgname = pkgbuild["pkgname"]

    # Check if necessary (this warns about binary version > aport version, so
    # call it even in force mode)
    ret = pmd.build.is_necessary(args, arch, pkgbuild)
    if force:
        ret = True

    if not ret and len(depends_built):
        logging.verbose(f"{pkgname}: depends on rebuilt package(s): "
                        f" {', '.join(depends_built)}")

    logging.verbose(pkgname + ": build necessary: " + str(ret))
    return ret


def init_buildenv(args, pkgbuild, arch, strict=False, force=False, cross=None,
                  suffix="native", skip_init_buildenv=False, src=None):
    """
    Build all dependencies, check if we need to build at all (otherwise we've
    just initialized the build environment for nothing) and then setup the
    whole build environment (abuild, gcc, dependencies, cross-compiler).

    :param cross: None, "native", "distcc", or "crossdirect"
    :param skip_init_buildenv: can be set to False to avoid initializing the
                               build environment. Use this when building
                               something during initialization of the build
                               environment (e.g. qemu aarch64 bug workaround)
    :param src: override source used to build the package with a local folder
    :returns: True when the build is necessary (otherwise False)
    """

    depends_arch = arch
    if cross == "native":
        depends_arch = pmd.config.arch_native

    # Build dependencies
    depends, built = build_depends(args, pkgbuild, depends_arch, strict)

    # Check if build is necessary
    if not is_necessary_warn_depends(args, pkgbuild, arch, force, built):
        return False

    # Install and configure abuild, ccache, gcc, dependencies
    if not skip_init_buildenv:
        pmd.build.init(args, suffix)
        pmd.build.other.configure_abuild(args, suffix)
        pmd.build.other.configure_ccache(args, suffix)
    if not strict and "pmd:strict" not in pkgbuild["options"] and len(depends):
        pmd.chroot.apk.install(args, depends, suffix)
    if src:
        pmd.chroot.apk.install(args, ["rsync"], suffix)

    # Cross-compiler init
    if cross:
        pmd.build.init_compiler(args, depends, cross, arch)
    if cross == "distcc":
        pmd.chroot.distccd.start(args, arch)
    if cross == "crossdirect":
        pmd.chroot.mount_native_into_foreign(args, suffix)

    return True


def get_gcc_version(args, arch):
    """
    Get the GCC version for a specific arch from parsing the right APKINDEX.
    We feed this to ccache, so it knows the right GCC version, when
    cross-compiling in a foreign arch chroot with distcc. See the "using
    ccache with other compiler wrappers" section of their man page:
    <https://linux.die.net/man/1/ccache>
    :returns: a string like "6.4.0-r5"
    """
    return pmd.parse.apkindex.package(args, "gcc-" + arch,
                                      pmd.config.arch_native)["version"]


def get_pkgver(original_pkgver, original_source=False, now=None):
    """
    Get the original pkgver when using the original source. Otherwise, get the
    pkgver with an appended suffix of current date and time. For example:
        _p20180218550502
    When appending the suffix, an existing suffix (e.g. _git20171231) gets
    replaced.

    :param original_pkgver: unmodified pkgver from the package's PKGBUILD.
    :param original_source: the original source is used instead of overriding
                            it with --src.
    :param now: use a specific date instead of current date (for test cases)
    """
    if original_source:
        return original_pkgver

    # Append current date
    no_suffix = original_pkgver.split("_", 1)[0]
    now = now if now else datetime.datetime.now()
    new_suffix = "_p" + now.strftime("%Y%m%d%H%M%S")
    return no_suffix + new_suffix


def override_source(args, pkgbuild, pkgver, src, suffix="native"):
    """
    Mount local source inside chroot and append new functions (prepare() etc.)
    to the PKGBUILD to make it use the local source.
    """
    if not src:
        return

    # Mount source in chroot
    mount_path = "/mnt/pmdebootstrap-source-override/"
    mount_path_outside = args.work + "/chroot_" + suffix + mount_path
    pmd.helpers.mount.bind(args, src, mount_path_outside, umount=True)

    # Delete existing append file
    append_path = "/tmp/PKGBUILD.append"
    append_path_outside = args.work + "/chroot_" + suffix + append_path
    if os.path.exists(append_path_outside):
        pmd.chroot.root(args, ["rm", append_path], suffix)

    # Add src path to pkgdesc, cut it off after max length
    pkgdesc = ("[" + src + "] " + pkgbuild["pkgdesc"])[:127]

    # Appended content
    append = """
             # ** Overrides below appended by pmdebootstrap for --src **

             pkgver=\"""" + pkgver + """\"
             pkgdesc=\"""" + pkgdesc + """\"
             _pmd_src_copy="/tmp/pmdebootstrap-local-source-copy"

             # Empty $source avoids patching in prepare()
             _pmd_source_original="$source"
             source=""
             sha512sums=""

             fetch() {
                 # Update source copy
                 msg "Copying source from host system: """ + src + """\"
                 rsync -a --exclude=".git/" --delete --ignore-errors --force \\
                     \"""" + mount_path + """\" "$_pmd_src_copy" || true

                 # Link local source files (e.g. kernel config)
                 mkdir "$srcdir"
                 local s
                 for s in $_pmd_source_original; do
                     is_remote "$s" || ln -sf "$startdir/$s" "$srcdir/"
                 done
             }

             unpack() {
                 ln -sv "$_pmd_src_copy" "$builddir"
             }
             """

    # Write and log append file
    with open(append_path_outside, "w", encoding="utf-8") as handle:
        for line in append.split("\n"):
            handle.write(line[13:].replace(" " * 4, "\t") + "\n")
    pmd.chroot.user(args, ["cat", append_path], suffix)

    # Append it to the PKGBUILD
    pkgbuild_path = "/home/pmos/build/PKGBUILD"
    shell_cmd = ("cat " + pkgbuild_path + " " + append_path + " > " +
                 append_path + "_")
    pmd.chroot.user(args, ["sh", "-c", shell_cmd], suffix)
    pmd.chroot.user(args, ["mv", append_path + "_", pkgbuild_path], suffix)


def mount_debports(args, destination, suffix="native"):
    """
    Mount debports.git in chroot.

    :param destination: mount point inside the chroot
    """
    outside_destination = args.work + "/chroot_" + suffix + destination
    pmd.helpers.mount.bind(args, args.debports, outside_destination, umount=True)


def link_to_git_dir(args, suffix):
    """
    Make /home/pmos/build/.git point to the .git dir from debports.git, with a
    symlink so abuild does not fail (#1841).

    abuild expects the current working directory to be a subdirectory of a
    cloned git repository (e.g. main/openrc from debports.git). If git is
    installed, it will try to get the last git commit from that repository, and
    place it in the resulting apk (.PKGINFO) as well as use the date from that
    commit as SOURCE_DATE_EPOCH (for reproducible builds).

    With that symlink, we actually make it use the last git commit from
    debports.git for SOURCE_DATE_EPOCH and have that in the resulting apk's
    .PKGINFO.
    """
    # Mount debports.git in chroot, in case the user did not use pmdebootstrap to
    # clone it (e.g. how we build on sourcehut). Do this here and not at the
    # initialization of the chroot, because the debports dir may not exist yet
    # at that point. Use umount=True, so we don't have an old path mounted
    # (some tests change the debports dir).
    destination = "/mnt/debports"
    mount_debports(args, destination, suffix)

    # Create .git symlink
    pmd.chroot.user(args, ["mkdir", "-p", "/home/pmos/build"], suffix)
    pmd.chroot.user(args, ["ln", "-sf", destination + "/.git",
                           "/home/pmos/build/.git"], suffix)


def run_abuild(args, pkgbuild, arch, strict=False, force=False, cross=None,
               suffix="native", src=None):
    """
    Set up all environment variables and construct the abuild command (all
    depending on the cross-compiler method and target architecture), copy
    the aport to the chroot and execute abuild.

    :param cross: None, "native", "distcc", or "crossdirect"
    :param src: override source used to build the package with a local folder
    :returns: (output, cmd, env), output is the destination apk path relative
              to the package folder ("x86_64/hello-1-r2.apk"). cmd and env are
              used by the test case, and they are the full abuild command and
              the environment variables dict generated in this function.
    """
    # Sanity check
    if cross == "native" and "!tracedeps" not in pkgbuild["options"]:
        logging.info("WARNING: Option !tracedeps is not set, but we're"
                     " cross-compiling in the native chroot. This will"
                     " probably fail!")

    # Pretty log message
    pkgver = get_pkgver(pkgbuild["pkgver"], src is None)
    output = (arch + "/" + pkgbuild["pkgname"] + "-" + pkgver +
              "-r" + pkgbuild["pkgrel"] + ".apk")
    message = "(" + suffix + ") build " + output
    if src:
        message += " (source: " + src + ")"
    logging.info(message)

    # Environment variables
    env = {"CARCH": arch,
           "SUDO_APK": "abuild-apk --no-progress"}
    if cross == "native":
        hostspec = pmd.parse.arch.debian_to_hostspec(arch)
        env["CROSS_COMPILE"] = hostspec + "-"
        env["CC"] = hostspec + "-gcc"
    if cross == "distcc":
        env["CCACHE_PREFIX"] = "distcc"
        env["CCACHE_PATH"] = f"/usr/lib/arch-bin-masquerade/{arch}:/usr/bin"
        env["CCACHE_COMPILERCHECK"] = "string:" + get_gcc_version(args, arch)
        env["DISTCC_HOSTS"] = "@127.0.0.1:/home/pmos/.distcc-sshd/distccd"
        env["DISTCC_SSH"] = ("ssh -o StrictHostKeyChecking=no -p" +
                             args.port_distccd)
        env["DISTCC_BACKOFF_PERIOD"] = "0"
        if not args.distcc_fallback:
            env["DISTCC_FALLBACK"] = "0"
        if args.verbose:
            env["DISTCC_VERBOSE"] = "1"
    if cross == "crossdirect":
        env["PATH"] = ":".join(["/native/usr/lib/crossdirect/" + arch,
                                pmd.config.chroot_path])
    if not args.ccache:
        env["CCACHE_DISABLE"] = "1"

    # Build the abuild command
    cmd = ["abuild", "-D", "postmarketOS"]
    if strict or "pmd:strict" in pkgbuild["options"]:
        if not strict:
            logging.debug(pkgbuild["pkgname"] + ": 'pmd:strict' found in"
                          " options, building in strict mode")
        cmd += ["-r"]  # install depends with abuild
    else:
        cmd += ["-d"]  # do not install depends with abuild
    if force:
        cmd += ["-f"]

    # Copy the aport to the chroot and build it
    pmd.build.copy_to_buildpath(args, pkgbuild["pkgname"], suffix)
    override_source(args, pkgbuild, pkgver, src, suffix)
    link_to_git_dir(args, suffix)
    pmd.chroot.user(args, cmd, suffix, "/home/pmos/build", env=env)
    return (output, cmd, env)


def finish(args, pkgbuild, arch, output, strict=False, suffix="native"):
    """
    Various finishing tasks that need to be done after a build.
    """
    # Verify output file
    channel = pmd.config.debports.read_config(args)["channel"]
    path = f"{args.work}/packages/{channel}/{output}"
    if not os.path.exists(path):
        raise RuntimeError("Package not found after build: " + path)

    # Clear APKINDEX cache (we only parse APKINDEX files once per session and
    # cache the result for faster dependency resolving, but after we built a
    # package we need to parse it again)
    pmd.parse.apkindex.clear_cache(f"{args.work}/packages/{channel}"
                                   f"/{arch}/APKINDEX.tar.gz")

    # Uninstall build dependencies (strict mode)
    if strict or "pmd:strict" in pkgbuild["options"]:
        logging.info("(" + suffix + ") uninstall build dependencies")
        pmd.chroot.user(args, ["abuild", "undeps"], suffix, "/home/pmos/build",
                        env={"SUDO_APK": "abuild-apk --no-progress"})
        # If the build depends contain postmarketos-keys or postmarketos-base,
        # abuild will have removed the postmarketOS repository key (pma#1230)
        pmd.chroot.init_keys(args)


def package(args, pkgname, arch=None, force=False, strict=False,
            skip_init_buildenv=False, src=None):
    """
    Build a package and its dependencies with Debian Linux' abuild.

    If this function is called multiple times on the same pkgname but first
    with force=False and then force=True the force argument will be ignored due
    to the package cache.
    See the skip_already_built() call below.

    :param pkgname: package name to be built, as specified in the PKGBUILD
    :param arch: architecture we're building for (default: native)
    :param force: always build, even if not necessary
    :param strict: avoid building with irrelevant dependencies installed by
                   letting abuild install and uninstall all dependencies.
    :param skip_init_buildenv: can be set to False to avoid initializing the
                               build environment. Use this when building
                               something during initialization of the build
                               environment (e.g. qemu aarch64 bug workaround)
    :param src: override source used to build the package with a local folder
    :returns: None if the build was not necessary
              output path relative to the packages folder ("armhf/ab-1-r2.apk")
    """
    # Once per session is enough
    arch = arch or pmd.config.arch_native
    if skip_already_built(pkgname, arch):
        return

    # Only build when PKGBUILD exists
    pkgbuild = get_pkgbuild(args, pkgname, arch)
    if not pkgbuild:
        return

    # Detect the build environment (skip unnecessary builds)
    if not check_build_for_arch(args, pkgname, arch):
        return
    suffix = pmd.build.autodetect.suffix(pkgbuild, arch)
    cross = pmd.build.autodetect.crosscompile(args, pkgbuild, arch, suffix)
    if not init_buildenv(args, pkgbuild, arch, strict, force, cross, suffix,
                         skip_init_buildenv, src):
        return

    # Build and finish up
    (output, cmd, env) = run_abuild(args, pkgbuild, arch, strict, force, cross,
                                    suffix, src)
    finish(args, pkgbuild, arch, output, strict, suffix)
    return output
