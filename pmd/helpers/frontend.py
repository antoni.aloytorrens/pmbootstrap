# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import glob
import json
import logging
import os
import sys

import pmd.build
import pmd.build.autodetect
import pmd.chroot
import pmd.chroot.other
import pmd.config
import pmd.export
import pmd.helpers.git
import pmd.helpers.lint
import pmd.helpers.logging
import pmd.helpers.pkgrel_bump
import pmd.helpers.debports
import pmd.helpers.repo
import pmd.helpers.repo_missing
import pmd.helpers.run
import pmd.helpers.aportupgrade
import pmd.helpers.status

def _parse_flavor(args, autoinstall=True):
    """
    Verify the flavor argument if specified, or return a default value.
    :param autoinstall: make sure that at least one kernel flavor is installed
    """
    # Install a kernel and get its "flavor", where flavor is a pmOS-specific
    # identifier that is typically in the form
    # "postmarketos-<manufacturer>-<device/chip>", e.g.
    # "postmarketos-qcom-sdm845"
    suffix = "rootfs_" + args.device
    flavor = pmd.chroot.other.kernel_flavor_installed(
        args, suffix, autoinstall)

    if not flavor:
        raise RuntimeError(
            "No kernel flavors installed in chroot " + suffix + "! Please let"
            " your device package depend on a package starting with 'linux-'.")
    return flavor


def _parse_suffix(args):
    if "rootfs" in args and args.rootfs:
        return "rootfs_" + args.device
    elif args.buildroot:
        if args.buildroot == "device":
            return "buildroot_" + args.deviceinfo["arch"]
        else:
            return "buildroot_" + args.buildroot
    elif args.suffix:
        return args.suffix
    else:
        return "native"


def aportgen(args):
    for package in args.packages:
        logging.info("Generate aport: " + package)
        pmd.aportgen.generate(args, package)


def build(args):
    # Strict mode: zap everything
    if args.strict:
        pmd.chroot.zap(args, False)

    if args.envkernel:
        pmd.build.envkernel.package_kernel(args)
        return

    # Set src and force
    src = os.path.realpath(os.path.expanduser(args.src[0])) \
        if args.src else None
    force = True if src else args.force
    if src and not os.path.exists(src):
        raise RuntimeError("Invalid path specified for --src: " + src)

    # Build all packages
    for package in args.packages:
        arch_package = args.arch or pmd.build.autodetect.arch(args, package)
        if not pmd.build.package(args, package, arch_package, force,
                                 args.strict, src=src):
            logging.info("NOTE: Package '" + package + "' is up to date. Use"
                         " 'pmdebootstrap build " + package + " --force'"
                         " if needed.")


def build_init(args):
    suffix = _parse_suffix(args)
    pmd.build.init(args, suffix)


def chroot(args):
    # Suffix
    suffix = _parse_suffix(args)
    if (args.user and suffix != "native" and
            not suffix.startswith("buildroot_")):
        raise RuntimeError("--user is only supported for native or"
                           " buildroot_* chroots.")
    if args.xauth and suffix != "native":
        raise RuntimeError("--xauth is only supported for native chroot.")

    # apk: check minimum version, install packages
    pmd.chroot.apk.check_min_version(args, suffix)
    if args.add:
        pmd.chroot.apk.install(args, args.add.split(","), suffix)

    # Xauthority
    env = {}
    if args.xauth:
        pmd.chroot.other.copy_xauthority(args)
        env["DISPLAY"] = os.environ.get("DISPLAY")
        env["XAUTHORITY"] = "/home/pmos/.Xauthority"

    # Install blockdevice
    if args.install_blockdev:
        size_boot = 128  # 128 MiB
        size_root = 4096  # 4 GiB
        size_reserve = 2048  # 2 GiB
        pmd.install.blockdevice.create_and_mount_image(args, size_boot,
                                                       size_root, size_reserve)

    # Run the command as user/root
    if args.user:
        logging.info("(" + suffix + ") % su pmos -c '" +
                     " ".join(args.command) + "'")
        pmd.chroot.user(args, args.command, suffix, output=args.output,
                        env=env)
    else:
        logging.info("(" + suffix + ") % " + " ".join(args.command))
        pmd.chroot.root(args, args.command, suffix, output=args.output,
                        env=env)


def config(args):
    keys = pmd.config.config_keys
    if args.name and args.name not in keys:
        logging.info("NOTE: Valid config keys: " + ", ".join(keys))
        raise RuntimeError("Invalid config key: " + args.name)

    cfg = pmd.config.load(args)
    if args.reset:
        if args.name is None:
            raise RuntimeError("config --reset requires a name to be given.")
        value = pmd.config.defaults[args.name]
        cfg["pmdebootstrap"][args.name] = value
        logging.info(f"Config changed to default: {args.name}='{value}'")
        pmd.config.save(args, cfg)
    elif args.value is not None:
        cfg["pmdebootstrap"][args.name] = args.value
        logging.info("Config changed: " + args.name + "='" + args.value + "'")
        pmd.config.save(args, cfg)
    elif args.name:
        value = cfg["pmdebootstrap"].get(args.name, "")
        print(value)
    else:
        cfg.write(sys.stdout)

    # Don't write the "Done" message
    pmd.helpers.logging.disable()


def repo_missing(args):
    missing = pmd.helpers.repo_missing.generate(args, args.arch, args.overview,
                                                args.package, args.built)
    print(json.dumps(missing, indent=4))


def index(args):
    pmd.build.index_repo(args)


def export(args):
    pmd.export.frontend(args)


def update(args):
    existing_only = not args.non_existing
    if not pmd.helpers.repo.update(args, args.arch, True, existing_only):
        logging.info("No APKINDEX files exist, so none have been updated."
                     " The pmdebootstrap command downloads the APKINDEX files on"
                     " demand.")
        logging.info("If you want to force downloading the APKINDEX files for"
                     " all architectures (not recommended), use:"
                     " pmdebootstrap update --non-existing")


def newpkgbuild(args):
    # Check for SRCURL usage
    is_url = False
    for prefix in ["http://", "https://", "ftp://"]:
        if args.pkgname_pkgver_srcurl.startswith(prefix):
            is_url = True
            break

    # Sanity check: -n is only allowed with SRCURL
    if args.pkgname and not is_url:
        raise RuntimeError("You can only specify a pkgname (-n) when using"
                           " SRCURL as last parameter.")

    # Passthrough: Strings (e.g. -d "my description")
    pass_through = []
    for entry in pmd.config.newpkgbuild_arguments_strings:
        value = getattr(args, entry[1])
        if value:
            pass_through += [entry[0], value]

    # Passthrough: Switches (e.g. -C for CMake)
    for entry in (pmd.config.newpkgbuild_arguments_switches_pkgtypes +
                  pmd.config.newpkgbuild_arguments_switches_other):
        if getattr(args, entry[1]) is True:
            pass_through.append(entry[0])

    # Passthrough: PKGNAME[-PKGVER] | SRCURL
    pass_through.append(args.pkgname_pkgver_srcurl)
    pmd.build.newpkgbuild(args, args.folder, pass_through, args.force)


def kconfig(args):
    if args.action_kconfig == "check":
        # Handle passing a file directly
        if args.file:
            if pmd.parse.kconfig.check_file(args.package,
                                            anbox=args.anbox,
                                            nftables=args.nftables,
                                            containers=args.containers,
                                            zram=args.zram,
                                            netboot=args.netboot,
                                            uefi=args.uefi,
                                            details=True):
                logging.info("kconfig check succeeded!")
                return
            raise RuntimeError("kconfig check failed!")

        # Default to all kernel packages
        packages = []
        if args.package:
            packages = [args.package]
        else:
            for aport in pmd.helpers.debports.get_list(args):
                if aport.startswith("linux-"):
                    packages.append(aport.split("linux-")[1])

        # Iterate over all kernels
        error = False
        skipped = 0
        packages.sort()
        for package in packages:
            if not args.force:
                pkgname = package if package.startswith("linux-") \
                    else "linux-" + package
                aport = pmd.helpers.debports.find(args, pkgname)
                pkgbuild = pmd.parse.pkgbuild(f"{aport}/PKGBUILD")
                if "!pmd:kconfigcheck" in pkgbuild["options"]:
                    skipped += 1
                    continue
            if not pmd.parse.kconfig.check(
                    args, package,
                    force_anbox_check=args.anbox,
                    force_apparmor_check=args.apparmor,
                    force_iwd_check=args.iwd,
                    force_nftables_check=args.nftables,
                    force_containers_check=args.containers,
                    force_zram_check=args.zram,
                    force_netboot_check=args.netboot,
                    force_uefi_check=args.uefi,
                    details=True):
                error = True

        # At least one failure
        if error:
            raise RuntimeError("kconfig check failed!")
        else:
            if skipped:
                logging.info("NOTE: " + str(skipped) + " kernel(s) was skipped"
                             " (consider 'pmdebootstrap kconfig check -f')")
            logging.info("kconfig check succeeded!")
    elif args.action_kconfig in ["edit", "migrate"]:
        if args.package:
            pkgname = args.package
        else:
            pkgname = args.deviceinfo["codename"]
        use_oldconfig = args.action_kconfig == "migrate"
        pmd.build.menuconfig(args, pkgname, use_oldconfig)


def deviceinfo_parse(args):
    # Default to all devices
    devices = args.devices
    if not devices:
        devices = pmd.helpers.devices.list_codenames(args)

    # Iterate over all devices
    kernel = args.deviceinfo_parse_kernel
    for device in devices:
        print(f"{device}, with kernel={kernel}:")
        print(json.dumps(pmd.parse.deviceinfo(args, device, kernel), indent=4,
                         sort_keys=True))


def pkgbuild_parse(args):
    # Default to all packages
    packages = args.packages
    if not packages:
        packages = pmd.helpers.debports.get_list(args)

    # Iterate over all packages
    for package in packages:
        print(package + ":")
        aport = pmd.helpers.debports.find(args, package)
        path = aport + "/PKGBUILD"
        print(json.dumps(pmd.parse.pkgbuild(path), indent=4,
                         sort_keys=True))


def apkindex_parse(args):
    result = pmd.parse.apkindex.parse(args.apkindex_path)
    if args.package:
        if args.package not in result:
            raise RuntimeError("Package not found in the APKINDEX: " +
                               args.package)
        result = result[args.package]
    print(json.dumps(result, indent=4))


def pkgrel_bump(args):
    would_bump = True
    if args.auto:
        would_bump = pmd.helpers.pkgrel_bump.auto(args, args.dry)
    else:
        # Each package must exist
        for package in args.packages:
            pmd.helpers.debports.find(args, package)

        # Increase pkgrel
        for package in args.packages:
            pmd.helpers.pkgrel_bump.package(args, package, dry=args.dry)

    if args.dry and would_bump:
        logging.info("Pkgrels of package(s) would have been bumped!")
        sys.exit(1)


def aportupgrade(args):
    if args.all or args.all_stable or args.all_git:
        pmd.helpers.aportupgrade.upgrade_all(args)
    else:
        # Each package must exist
        for package in args.packages:
            pmd.helpers.debports.find(args, package)

        # Check each package for a new version
        for package in args.packages:
            pmd.helpers.aportupgrade.upgrade(args, package)


def qemu(args):
    pmd.qemu.run(args)


def shutdown(args):
    pmd.chroot.shutdown(args)


def stats(args):
    # Chroot suffix
    suffix = "native"
    if args.arch != pmd.config.arch_native:
        suffix = "buildroot_" + args.arch

    # Install ccache and display stats
    pmd.chroot.apk.install(args, ["ccache"], suffix)
    logging.info("(" + suffix + ") % ccache -s")
    pmd.chroot.user(args, ["ccache", "-s"], suffix, output="stdout")


def work_migrate(args):
    # do nothing (pmd/__init__.py already did the migration)
    pmd.helpers.logging.disable()


def log(args):
    if args.clear_log:
        pmd.helpers.run.user(args, ["truncate", "-s", "0", args.log])
    pmd.helpers.run.user(args, ["tail", "-n", args.lines, "-F", args.log],
                         output="tui")


def log_distccd(args):
    logpath = "/home/pmos/distccd.log"
    if args.clear_log:
        pmd.chroot.user(args, ["truncate", "-s", "0", logpath])
    pmd.chroot.user(args, ["tail", "-n", args.lines, "-f", logpath],
                    output="tui")


def zap(args):
    pmd.chroot.zap(args, dry=args.dry, http=args.http,
                   distfiles=args.distfiles, pkgs_local=args.pkgs_local,
                   pkgs_local_mismatch=args.pkgs_local_mismatch,
                   pkgs_online_mismatch=args.pkgs_online_mismatch,
                   rust=args.rust, netboot=args.netboot)

    # Don't write the "Done" message
    pmd.helpers.logging.disable()


def pull(args):
    failed = []
    for repo in pmd.config.git_repos.keys():
        if pmd.helpers.git.pull(args, repo) < 0:
            failed.append(repo)

    if not failed:
        return True

    logging.info("---")
    logging.info("WARNING: failed to update: " + ", ".join(failed))
    logging.info("")
    logging.info("'pmdebootstrap pull' will only update the repositories, if:")
    logging.info("* they are on an officially supported branch (e.g. master)")
    logging.info("* the history is not conflicting (fast-forward is possible)")
    logging.info("* the git workdirs are clean")
    logging.info("You have changed mentioned repositories, so they don't meet")
    logging.info("these conditions anymore.")
    logging.info("")
    logging.info("Fix and try again:")
    for name_repo in failed:
        logging.info("* " + pmd.helpers.git.get_path(args, name_repo))
    logging.info("---")
    return False


def lint(args):
    packages = args.packages
    if not packages:
        packages = pmd.helpers.debports.get_list(args)

    pmd.helpers.lint.check(args, packages)


def status(args):
    if not pmd.helpers.status.print_status(args, args.details):
        sys.exit(1)
