# Copyright 2022 Danct12 <danct12@disroot.org>
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import os

import pmd.chroot
import pmd.chroot.apk
import pmd.build
import pmd.helpers.run
import pmd.helpers.debports


def check(args, pkgnames):
    """
    Run pkgbuild-lint on the supplied packages

    :param pkgnames: Names of the packages to lint
    """
    pmd.chroot.apk.install(args, ["atools"])
    pmd.build.init(args)

    # Mount debports.git inside the chroot so that we don't have to copy the
    # package folders
    debports = "/mnt/debports"
    pmd.build.mount_debports(args, debports)

    # Locate all PKGBUILDs and make the paths be relative to the debports
    # root
    pkgbuilds = []
    for pkgname in pkgnames:
        aport = pmd.helpers.debports.find(args, pkgname)
        if not os.path.exists(aport + "/PKGBUILD"):
            raise ValueError("Path does not contain an PKGBUILD file:" +
                             aport)
        relpath = os.path.relpath(aport, args.debports)
        pkgbuilds.append(f"{relpath}/PKGBUILD")

    # Run pkgbuild-lint in chroot from the debports mount point. This will
    # print a nice source identifier à la "./cross/grub-x86/PKGBUILD" for
    # each violation.
    pkgstr = ", ".join(pkgnames)
    logging.info(f"(native) linting {pkgstr} with pkgbuild-lint")
    options = pmd.config.pkgbuild_custom_valid_options
    return pmd.chroot.root(args, ["pkgbuild-lint"] + pkgbuilds,
                           check=False, output="stdout",
                           output_return=True,
                           working_dir=debports,
                           env={"CUSTOM_VALID_OPTIONS": " ".join(options)})
