# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import glob
import os
import socket
from contextlib import closing

import pmd.chroot
import pmd.chroot.distccd
import pmd.helpers.mount
import pmd.parse.arch


def kill_adb(args):
    """
    Kill adb daemon if it's running.
    """
    port = 5038
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        if sock.connect_ex(("127.0.0.1", port)) == 0:
            pmd.chroot.root(args, ["adb", "-P", str(port), "kill-server"])


def shutdown_cryptsetup_device(args, name):
    """
    :param name: cryptsetup device name, usually "pm_crypt" in pmdebootstrap
    """
    if not os.path.exists(args.work + "/chroot_native/dev/mapper/" + name):
        return
    pmd.chroot.apk.install(args, ["cryptsetup"])
    status = pmd.chroot.root(args, ["cryptsetup", "status", name],
                             output_return=True, check=False)
    if not status:
        logging.warning("WARNING: Failed to run cryptsetup to get the status"
                        " for " + name + ", assuming it is not mounted"
                        " (shutdown fails later if it is)!")
        return

    if status.startswith("/dev/mapper/" + name + " is active."):
        pmd.chroot.root(args, ["cryptsetup", "luksClose", name])
    elif status.startswith("/dev/mapper/" + name + " is inactive."):
        # When "cryptsetup status" fails, the device is not mounted and we
        # have a left over file (#83)
        pmd.chroot.root(args, ["rm", "/dev/mapper/" + name])
    else:
        raise RuntimeError("Failed to parse 'cryptsetup status' output!")


def shutdown(args, only_install_related=False):
    pmd.chroot.distccd.stop(args)

    # Stop adb server
    kill_adb(args)

    # Umount installation-related paths (order is important!)
    pmd.helpers.mount.umount_all(args, args.work +
                                 "/chroot_native/mnt/install")
    shutdown_cryptsetup_device(args, "pm_crypt")

    # Umount all losetup mounted images
    chroot = args.work + "/chroot_native"
    if pmd.helpers.mount.ismount(chroot + "/dev/loop-control"):
        pattern = chroot + "/home/pmos/rootfs/*.img"
        for path_outside in glob.glob(pattern):
            path = path_outside[len(chroot):]
            pmd.install.losetup.umount(args, path, auto_init=False)

    # Umount device rootfs and installer chroots
    for prefix in ["rootfs", "installer"]:
        path = f"{args.work}/chroot_{prefix}_{args.device}"
        if os.path.exists(path):
            pmd.helpers.mount.umount_all(args, path)

    # Remove "in-pmdebootstrap" marker from all chroots. This marker indicates
    # that pmdebootstrap has set up all mount points etc. to run programs inside
    # the chroots, but we want it gone afterwards (e.g. when the chroot
    # contents get copied to a rootfs / installer image, or if creating an
    # android recovery zip from its contents).
    for marker in glob.glob(f"{args.work}/chroot_*/in-pmdebootstrap"):
        pmd.helpers.run.root(args, ["rm", marker])

    if not only_install_related:
        # Umount all folders inside args.work
        # The folders are explicitly iterated over, so folders symlinked inside
        # args.work get umounted as well (used in test_pkgrel_bump.py, #1595)
        for path in glob.glob(args.work + "/*"):
            pmd.helpers.mount.umount_all(args, path)

        # Clean up the rest
        for arch in pmd.config.build_device_architectures:
            if pmd.parse.arch.cpu_emulation_required(arch):
                pmd.chroot.binfmt.unregister(args, arch)
        logging.debug("Shutdown complete")
